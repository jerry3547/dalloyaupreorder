//
//  SQLiteMaster.swift
//  DalloyauPreOrder
//
//  Created by jerry on 26/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import SQLite

class SQLiteMaster
{
    private var objConnection : Connection
    private var objSystemLog : SystemIOMaster.SystemLog = SystemIOMaster.SystemLog()
    private var strDataPath : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/SQLiteDatabase.sqlite"
    private var strActionLog : String = ""
    private var strSQLScript : String = ""
    
    init() {
        if (FileManager.default.fileExists(atPath : strDataPath) != true)
        {
            objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite file not found")
        }
        
        objConnection = try! SQLite.Connection(strDataPath)
    }
    
    func CheckTable() {
        objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "--Start--")
        
        // tblSettingConcurrenceControl //
        strSQLScript = "CREATE TABLE IF NOT EXISTS tblSettingConcurrenceControl (intDisplayOrder Int64, strFunctionGroup TEXT, strFunctionObject TEXT, strFunctionValue TEXT, dtmModifiedDate DATETIME, PRIMARY KEY (strFunctionGroup, strFunctionObject));"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        strActionLog = "tblSettingConcurrenceControl : \(GetTableRowCount(pstrTableName : "tblSettingConcurrenceControl"))"
        objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : strActionLog)
        
        // tblPOSStore //
        strSQLScript = "CREATE TABLE IF NOT EXISTS tblPOSStore (strStoreCode TEXT, strStoreDesc TEXT, strStoreAddress TEXT, strStorePhone TEXT, strStoreBusinessHour TEXT, strStoreImageURL TEXT, PRIMARY KEY(strStoreCode));"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        strActionLog = "tblPOSStore : \(GetTableRowCount(pstrTableName : "tblPOSStore"))"
        objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : strActionLog)
        
        // tblPOSLollipopMessage //
        strSQLScript = "CREATE TABLE IF NOT EXISTS tblPOSLollipopMessage (strMessageID TEXT, strMessageDesc TEXT, strParentID TEXT, intDisplayOrder INTEGER, PRIMARY KEY(strMessageID));"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        strActionLog = "tblPOSStore : \(GetTableRowCount(pstrTableName : "tblPOSStore"))"
        objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : strActionLog)
        
        // tblPOSItemMaster //
        strSQLScript = "CREATE TABLE IF NOT EXISTS tblPOSItemMaster (strItemCode TEXT, strItemDesc TEXT, strItemRemark TEXT, decOriginAmt REAL, decSalesAmt REAL, intDeliveryDay INTEGER, dtmPublicDateFm TEXT, dtmPublicDateTo TEXT, PRIMARY KEY (strItemCode));"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        strActionLog = "tblPOSItemMaster : \(GetTableRowCount(pstrTableName : "tblPOSItemMaster"))"
        objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : strActionLog)
        
        // tblPOSItemImage //
        strSQLScript = "CREATE TABLE IF NOT EXISTS tblPOSItemImage (strItemCode TEXT, strImageURL TEXT, PRIMARY KEY(strItemCode, strImageURL));"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        strActionLog = "tblPOSItemImage : \(GetTableRowCount(pstrTableName : "tblPOSItemImage"))"
        objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : strActionLog)
        
        // tblPOSItemTypeConfig //
        strSQLScript = "CREATE TABLE IF NOT EXISTS tblPOSItemTypeConfig (strItemType TEXT, strTypeCode TEXT, strTypeDesc TEXT, strTypeRemark TEXT, intDisplayOrder INTEGER, intAddOnCount INTEGER, dtmPublicDateFm TEXT, dtmPublicDateTo TEXT, PRIMARY KEY (strItemType, strTypeCode));"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        strActionLog = "tblPOSItemTypeConfig : \(GetTableRowCount(pstrTableName : "tblPOSItemTypeConfig"))"
        objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : strActionLog)
        
        // tblPOSItemCakeConfigAttribute //
        strSQLScript = "CREATE TABLE IF NOT EXISTS tblPOSItemCakeConfigAttribute (strCakeCode TEXT, strItemCode TEXT, strCakeAttributeDescPrint TEXT, strCakeAttributeDescFlavor TEXT, strCakeAttributeDescSize TEXT, intDisplayOrder INTEGER, PRIMARY KEY (strItemCode));"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        strActionLog = "tblPOSItemCakeConfigAttribute : \(GetTableRowCount(pstrTableName : "tblPOSItemCakeConfigAttribute"))"
        objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : strActionLog)
        
        objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "--End--")
    }
    
    func GetTableList() -> [String] {
        var TableList : [String] = []
        strSQLScript = "SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;"
        
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return TableList}
        for tblDataRow : Array in tblDataTable
        {
            TableList.append((tblDataRow[0] ?? "") as! String)
        }
        
        return TableList
    }
    
    func GetColumnList(pstrTableName : String) -> [SQLiteTableColumn] {
        var SQLiteTableColumnList : [SQLiteTableColumn] = []
        strSQLScript = "PRAGMA table_info('\(pstrTableName)');"
        
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return SQLiteTableColumnList}
        for tblDataRow : Array in tblDataTable
        {
            var Column : SQLiteTableColumn = SQLiteTableColumn()
            Column.intColumnID = Int((tblDataRow[0] ?? 0) as! Int64)
            Column.strColumnName = (tblDataRow[1] ?? "") as! String
            Column.strColumnType = (tblDataRow[2] ?? "") as! String
            SQLiteTableColumnList.append(Column)
        }
        
        return SQLiteTableColumnList
    }
    
    func GetTableData(pstrTableName : String) -> DataTableContent{
        var dataTableSource : DataTableContent = []
        let SQLiteTableColumnList : [SQLiteTableColumn] = GetColumnList(pstrTableName: pstrTableName)
        strSQLScript = "SELECT "
        for SQLiteTableColumn : SQLiteTableColumn in SQLiteTableColumnList
        {
            strSQLScript = strSQLScript + "\(SQLiteTableColumn.strColumnName), "
        }
        strSQLScript = strSQLScript + "FROM \(pstrTableName)"
        strSQLScript = strSQLScript.replacingOccurrences(of: ", FROM", with: " FROM")
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return dataTableSource}
        
        for tblDataRow : Array in tblDataTable
        {
            var dataTableRow : DataTableRow = DataTableRow()
            for SQLiteTableColumn : SQLiteTableColumn in SQLiteTableColumnList
            {
                switch(SQLiteTableColumn.strColumnType)
                {
                case "INT64", "Int64":
                    let intValue : Int64 = (tblDataRow[SQLiteTableColumn.intColumnID] ?? 0) as! Int64
                    dataTableRow.append(DataTableValueType.int(Int(intValue)))
                    break
                case "INTEGER":
                    let intValue : Int = Int((tblDataRow[SQLiteTableColumn.intColumnID] ?? 0) as! Int64)
                    dataTableRow.append(DataTableValueType.int(intValue))
                    break
                case "REAL":
                    let dblValue : Double = (tblDataRow[SQLiteTableColumn.intColumnID] ?? 0.0) as! Double
                    dataTableRow.append(DataTableValueType.double(dblValue))
                    break
                case "NUMERIC", "TEXT", "DATETIME":
                    let strValue : String = (tblDataRow[SQLiteTableColumn.intColumnID] ?? "") as! String
                    dataTableRow.append(DataTableValueType.string(strValue))
                    break
                case "BOOL":
                    let strValue : String = (tblDataRow[SQLiteTableColumn.intColumnID] ?? "true") as! String
                    dataTableRow.append(DataTableValueType.string(strValue))
                default:
                    print("SQLiteTableColumn.strColumnType : \(SQLiteTableColumn.strColumnType)")
                    break
                }
            }
            
            dataTableSource.append(dataTableRow)
        }
        
        return dataTableSource
    }
    
    func GetTableRowCount(pstrTableName : String) -> String {
        let objFormatter : NumberFormatter = NumberFormatter()
        objFormatter.numberStyle = .currency
        objFormatter.currencySymbol = ""
        objFormatter.maximumFractionDigits = 0
        objFormatter.minimumFractionDigits = 0
        
        strSQLScript = "SELECT COUNT(*) FROM \(pstrTableName);"
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return ""}
        
        for tblDataRow : Array in tblDataTable
        {
            return "\(objFormatter.string(for : (tblDataRow[0] ?? 0) as! Int64)!)"
        }
        return ""
    }
    
    // tblSettingConcurrenceControl //
    func UpdateSettingConcurrenceControl(ptblSettingConcurrenceControl : StructMaster.Table.tblSettingConcurrenceControlStructList.tblSettingConcurrenceControlStruct) {
        strSQLScript = "INSERT OR IGNORE INTO tblSettingConcurrenceControl (intDisplayOrder, strFunctionGroup, strFunctionObject, strFunctionValue, dtmModifiedDate) "
        strSQLScript = strSQLScript + "VALUES ("
        strSQLScript = strSQLScript + "'\(ptblSettingConcurrenceControl.intDisplayOrder)', "
        strSQLScript = strSQLScript + "'\(ptblSettingConcurrenceControl.strFunctionGroup)', "
        strSQLScript = strSQLScript + "'\(ptblSettingConcurrenceControl.strFunctionObject)', "
        strSQLScript = strSQLScript + "'\(ptblSettingConcurrenceControl.strFunctionValue.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(GetTimeString(pdtmTime : Date()))' "
        strSQLScript = strSQLScript + ");"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        
        strSQLScript = "UPDATE tblSettingConcurrenceControl SET "
        strSQLScript = strSQLScript + "intDisplayOrder='\(ptblSettingConcurrenceControl.intDisplayOrder)', "
        strSQLScript = strSQLScript + "strFunctionValue='\(ptblSettingConcurrenceControl.strFunctionValue.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "dtmModifiedDate='\(GetTimeString(pdtmTime : Date()))' "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "strFunctionGroup='\(ptblSettingConcurrenceControl.strFunctionGroup)' "
        strSQLScript = strSQLScript + "AND strFunctionObject='\(ptblSettingConcurrenceControl.strFunctionObject)';"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
    }
    
    func ReadSettingConcurrenceControlValue(pstrFunctionGroup : String, pstrFunctionObject : String) -> String {
        strSQLScript = "SELECT strFunctionValue "
        strSQLScript = strSQLScript + "FROM tblSettingConcurrenceControl "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "strFunctionGroup='\(pstrFunctionGroup)' "
        strSQLScript = strSQLScript + "AND strFunctionObject='\(pstrFunctionObject)';"
        
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return ""}
        do {
            for tblDataRow : Array in tblDataTable
            {
                return "\((tblDataRow[0] ?? "") as! String)"
            }
        }
        
        return ""
    }
    
    // tblPOSStore //
    func UpdatePOSStore(ptblPOSStore : StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct) {
        strSQLScript = "INSERT OR IGNORE INTO tblPOSStore (strStoreCode, strStoreDesc, strStoreAddress, strStorePhone, strStoreBusinessHour, strStoreImageURL) "
        strSQLScript = strSQLScript + "VALUES ("
        strSQLScript = strSQLScript + "'\(ptblPOSStore.strStoreCode)', "
        strSQLScript = strSQLScript + "'\(ptblPOSStore.strStoreDesc.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(ptblPOSStore.strStoreAddress.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(ptblPOSStore.strStorePhone.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(ptblPOSStore.strStoreBusinessHour.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(ptblPOSStore.strStoreImageURL)' "
        strSQLScript = strSQLScript + ");"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        
        strSQLScript = "UPDATE tblPOSStore SET "
        strSQLScript = strSQLScript + "strStoreDesc='\(ptblPOSStore.strStoreDesc.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "strStoreAddress='\(ptblPOSStore.strStoreAddress.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "strStorePhone='\(ptblPOSStore.strStorePhone.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "strStoreBusinessHour='\(ptblPOSStore.strStoreBusinessHour.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "strStoreImageURL='\(ptblPOSStore.strStoreImageURL)' "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "strStoreCode='\(ptblPOSStore.strStoreCode)';"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
    }
    
    func ReadPOSStore() -> StructMaster.Table.tblPOSStoreStructList {
        var tblPOSStoreList : StructMaster.Table.tblPOSStoreStructList = StructMaster.Table.tblPOSStoreStructList()
        strSQLScript = "SELECT  "
        strSQLScript = strSQLScript + "strStoreCode, strStoreDesc, strStoreAddress, strStorePhone, strStoreBusinessHour, strStoreImageURL "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSStore;"
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return tblPOSStoreList}
        
        for tblDataRow : Array in tblDataTable
        {
            var tblPOSStore : StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct = StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct()
            tblPOSStore.strStoreCode = (tblDataRow[0] ?? "") as! String
            tblPOSStore.strStoreDesc = (tblDataRow[1] ?? "") as! String
            tblPOSStore.strStoreAddress = (tblDataRow[2] ?? "") as! String
            tblPOSStore.strStorePhone = (tblDataRow[3] ?? "") as! String
            tblPOSStore.strStoreBusinessHour = (tblDataRow[4] ?? "") as! String
            tblPOSStore.strStoreImageURL = (tblDataRow[5] ?? "") as! String
            tblPOSStoreList.Detail.append(tblPOSStore)
        }
        
        return tblPOSStoreList
    }
    
    func ReadPOSStore(pstrStoreCode : String) -> StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct {
        strSQLScript = "SELECT  "
        strSQLScript = strSQLScript + "strStoreCode, strStoreDesc, strStoreAddress, strStorePhone, strStoreBusinessHour, strStoreImageURL "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSStore "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "strStoreCode='\(pstrStoreCode)';"
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct()}
        
        for tblDataRow : Array in tblDataTable
        {
            var tblPOSStore : StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct = StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct()
            tblPOSStore.strStoreCode = (tblDataRow[0] ?? "") as! String
            tblPOSStore.strStoreDesc = (tblDataRow[1] ?? "") as! String
            tblPOSStore.strStoreAddress = (tblDataRow[2] ?? "") as! String
            tblPOSStore.strStorePhone = (tblDataRow[3] ?? "") as! String
            tblPOSStore.strStoreBusinessHour = (tblDataRow[4] ?? "") as! String
            tblPOSStore.strStoreImageURL = (tblDataRow[5] ?? "") as! String
            return tblPOSStore
        }
        
        return StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct()
    }
    
    // tblPOSLollipopMessage //
    func UpdatePOSLollipopMessage(pblPOSLollipopMessage : StructMaster.Table.tblPOSLollipopMessageStructList.tblPOSLollipopMessageStruct) {
        strSQLScript = "INSERT OR IGNORE INTO tblPOSLollipopMessage (strMessageID, strMessageDesc, strParentID, intDisplayOrder) "
        strSQLScript = strSQLScript + "VALUES ("
        strSQLScript = strSQLScript + "'\(pblPOSLollipopMessage.strMessageID)', "
        strSQLScript = strSQLScript + "'\(pblPOSLollipopMessage.strMessageDesc.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(pblPOSLollipopMessage.strParentID)', "
        strSQLScript = strSQLScript + "'\(pblPOSLollipopMessage.intDisplayOrder)' "
        strSQLScript = strSQLScript + ");"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        
        strSQLScript = "UPDATE tblPOSLollipopMessage SET "
        strSQLScript = strSQLScript + "strMessageDesc='\(pblPOSLollipopMessage.strMessageDesc.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "strParentID='\(pblPOSLollipopMessage.strParentID)', "
        strSQLScript = strSQLScript + "intDisplayOrder='\(pblPOSLollipopMessage.intDisplayOrder)' "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "strMessageID='\(pblPOSLollipopMessage.strMessageID)';"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
    }
    
    // tlbPOItemMaster //
    func UpdatePOSItem(ptblPOSItem : StructMaster.Table.tblPOSItemStructList.tblPOSItemStruct){
        strSQLScript = "INSERT OR IGNORE INTO tblPOSItemMaster (strItemCode, strItemDesc, strItemRemark, decOriginAmt, decSalesAmt, intDeliveryDay, dtmPublicDateFm, dtmPublicDateTo) "
        strSQLScript = strSQLScript + "VALUES ("
        strSQLScript = strSQLScript + "'\(ptblPOSItem.strItemCode)', "
        strSQLScript = strSQLScript + "'\(ptblPOSItem.strItemDesc.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(ptblPOSItem.strItemRemark.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(ptblPOSItem.decOriginAmt)', "
        strSQLScript = strSQLScript + "'\(ptblPOSItem.decSalesAmt)', "
        strSQLScript = strSQLScript + "'\(ptblPOSItem.intDeliveryDay)', "
        strSQLScript = strSQLScript + "'\(GetDateString(pdtmDate: ptblPOSItem.dtmPublicDateFm))', "
        strSQLScript = strSQLScript + "'\(GetDateString(pdtmDate : ptblPOSItem.dtmPublicDateTo))' "
        strSQLScript = strSQLScript + ");"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        
        strSQLScript = "UPDATE tblPOSItemMaster SET "
        strSQLScript = strSQLScript + "strItemDesc='\(ptblPOSItem.strItemDesc.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "strItemRemark='\(ptblPOSItem.strItemRemark.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "decOriginAmt='\(ptblPOSItem.decOriginAmt)', "
        strSQLScript = strSQLScript + "decSalesAmt='\(ptblPOSItem.decSalesAmt)', "
        strSQLScript = strSQLScript + "intDeliveryDay='\(ptblPOSItem.intDeliveryDay)', "
        strSQLScript = strSQLScript + "dtmPublicDateFm='\(GetDateString(pdtmDate : ptblPOSItem.dtmPublicDateFm))', "
        strSQLScript = strSQLScript + "dtmPublicDateTo='\(GetDateString(pdtmDate : ptblPOSItem.dtmPublicDateTo))' "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "strItemCode='\(ptblPOSItem.strItemCode)';"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
    }

    func ReadPOSItem(pstrItemCode : String) -> StructMaster.Table.tblPOSItemStructList.tblPOSItemStruct {
        var tblPOSItem : StructMaster.Table.tblPOSItemStructList.tblPOSItemStruct = StructMaster.Table.tblPOSItemStructList.tblPOSItemStruct()
        strSQLScript = "SELECT  "
        strSQLScript = strSQLScript + "strItemCode, strItemDesc, strItemRemark, decOriginAmt, decSalesAmt, intDeliveryDay, dtmPublicDateFm, dtmPublicDateTo  "
        strSQLScript = strSQLScript + "FROM  "
        strSQLScript = strSQLScript + "tblPOSItemMaster  "
        strSQLScript = strSQLScript + "WHERE  "
        strSQLScript = strSQLScript + "strItemCode='\(pstrItemCode)';"
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return tblPOSItem}
        
        for tblDataRow : Array in tblDataTable
        {
            tblPOSItem.strItemCode = (tblDataRow[0] ?? "") as! String
            tblPOSItem.strItemDesc = (tblDataRow[1] ?? "") as! String
            tblPOSItem.strItemRemark = (tblDataRow[2] ?? "") as! String
            tblPOSItem.decOriginAmt = Double((tblDataRow[3] ?? 0.0) as! Double)
            tblPOSItem.decSalesAmt = Double((tblDataRow[4] ?? 0.0) as! Double)
            tblPOSItem.intDeliveryDay = Int64((tblDataRow[5] ?? 0) as! Int64)
            tblPOSItem.dtmPublicDateFm = BindingDate(objBinding: tblDataRow[6] ?? "")
            tblPOSItem.dtmPublicDateTo = BindingDate(objBinding: tblDataRow[7] ?? "")
        }
        
        return tblPOSItem
    }

    // tblPOSItemTypeConfig //
    func UpdatePOSItemTypeConfig(ptblPOSItemTypeConfig : StructMaster.Table.tblPOSItemTypeConfigStructList.tblPOSItemTypeConfigStruct) {
        strSQLScript = "INSERT OR IGNORE INTO tblPOSItemTypeConfig (strItemType, strTypeCode, strTypeDesc, strtypeRemark, intAddOnCount, intDisplayOrder, dtmPublicDateFm, dtmPublicDateTo) "
        strSQLScript = strSQLScript + "VALUES ("
        strSQLScript = strSQLScript + "'\(ptblPOSItemTypeConfig.strItemType)', "
        strSQLScript = strSQLScript + "'\(ptblPOSItemTypeConfig.strTypeCode)', "
        strSQLScript = strSQLScript + "'\(ptblPOSItemTypeConfig.strTypeDesc.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(ptblPOSItemTypeConfig.strTypeRemark.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(ptblPOSItemTypeConfig.intAddOnCount)', "
        strSQLScript = strSQLScript + "'\(ptblPOSItemTypeConfig.intDisplayOrder)', "
        strSQLScript = strSQLScript + "'\(GetDateString(pdtmDate: ptblPOSItemTypeConfig.dtmPublicDateFm))', "
        strSQLScript = strSQLScript + "'\(GetDateString(pdtmDate : ptblPOSItemTypeConfig.dtmPublicDateTo))' "
        strSQLScript = strSQLScript + ");"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        
        strSQLScript = "UPDATE tblPOSItemTypeConfig SET "
        strSQLScript = strSQLScript + "strTypeDesc='\(ptblPOSItemTypeConfig.strTypeDesc.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "strTypeRemark='\(ptblPOSItemTypeConfig.strTypeRemark.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "intAddOnCount='\(ptblPOSItemTypeConfig.intAddOnCount)', "
        strSQLScript = strSQLScript + "intDisplayOrder='\(ptblPOSItemTypeConfig.intDisplayOrder)', "
        strSQLScript = strSQLScript + "dtmPublicDateFm='\(GetDateString(pdtmDate : ptblPOSItemTypeConfig.dtmPublicDateFm))', "
        strSQLScript = strSQLScript + "dtmPublicDateTo='\(GetDateString(pdtmDate : ptblPOSItemTypeConfig.dtmPublicDateTo))' "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "strItemType='\(ptblPOSItemTypeConfig.strItemType)' "
        strSQLScript = strSQLScript + "AND strTypeCode='\(ptblPOSItemTypeConfig.strTypeCode)';"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
    }
    
    func ReadPOSItemTypeConfig(pstrItemType : String, pstrTypeCode : String) -> StructMaster.Table.tblPOSItemTypeConfigStructList.tblPOSItemTypeConfigStruct {
        var tblPOSItemTypeConfig : StructMaster.Table.tblPOSItemTypeConfigStructList.tblPOSItemTypeConfigStruct = StructMaster.Table.tblPOSItemTypeConfigStructList.tblPOSItemTypeConfigStruct()
        strSQLScript = "SELECT  "
        strSQLScript = strSQLScript + "strTypeCode, strTypeCode, strTypeDesc, strtypeRemark, intAddOnCount, intDisplayOrder, dtmPublicDateFm, dtmPublicDateTo "
        strSQLScript = strSQLScript + "FROM  "
        strSQLScript = strSQLScript + "tblPOSItemTypeConfig  "
        strSQLScript = strSQLScript + "WHERE  "
        strSQLScript = strSQLScript + "strItemType='\(pstrItemType)' "
        strSQLScript = strSQLScript + "AND strTypeCode='\(pstrTypeCode)';"
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return tblPOSItemTypeConfig}
        
        for tblDataRow : Array in tblDataTable
        {
            tblPOSItemTypeConfig.strItemType = (tblDataRow[0] ?? "") as! String
            tblPOSItemTypeConfig.strTypeCode = (tblDataRow[1] ?? "") as! String
            tblPOSItemTypeConfig.strTypeDesc = (tblDataRow[2] ?? "") as! String
            tblPOSItemTypeConfig.strTypeRemark = (tblDataRow[3] ?? "") as! String
            tblPOSItemTypeConfig.intDisplayOrder = Int64((tblDataRow[4] ?? 0) as! Int64)
            tblPOSItemTypeConfig.dtmPublicDateFm = BindingDate(objBinding: tblDataRow[6] ?? "")
            tblPOSItemTypeConfig.dtmPublicDateTo = BindingDate(objBinding: tblDataRow[7] ?? "")
        }
        return tblPOSItemTypeConfig
    }
    
    // tblPOSItemCakeConfigAttribute //
    func UpdatePOSItemCakeConfigAttribute(ptblPOSItemCakeConfigAttribute : StructMaster.Table.tblPOSItemCakeConfigAttributeStructList.tblPOSItemCakeConfigAttributeStruct) {
        strSQLScript = "INSERT OR IGNORE INTO tblPOSItemCakeConfigAttribute (strCakeCode, strItemCode, strCakeAttributeDescPrint, strCakeAttributeDescFlavor, strCakeAttributeDescSize, intDisplayOrder) "
        strSQLScript = strSQLScript + "VALUES ("
        strSQLScript = strSQLScript + "'\(ptblPOSItemCakeConfigAttribute.strCakeCode)', "
        strSQLScript = strSQLScript + "'\(ptblPOSItemCakeConfigAttribute.strItemCode)', "
        strSQLScript = strSQLScript + "'\(ptblPOSItemCakeConfigAttribute.strCakeAttributeDescPrint.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(ptblPOSItemCakeConfigAttribute.strCakeAttributeDescFlavor.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(ptblPOSItemCakeConfigAttribute.strCakeAttributeDescSize.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "'\(ptblPOSItemCakeConfigAttribute.intDisplayOrder)' "
        strSQLScript = strSQLScript + ");"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
        
        strSQLScript = "UPDATE tblPOSItemCakeConfigAttribute SET "
        strSQLScript = strSQLScript + "strCakeCode='\(ptblPOSItemCakeConfigAttribute.strCakeCode)', "
        strSQLScript = strSQLScript + "strCakeAttributeDescPrint='\(ptblPOSItemCakeConfigAttribute.strCakeAttributeDescPrint.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "strCakeAttributeDescFlavor='\(ptblPOSItemCakeConfigAttribute.strCakeAttributeDescFlavor.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "strCakeAttributeDescSize='\(ptblPOSItemCakeConfigAttribute.strCakeAttributeDescSize.replacingOccurrences(of: "'", with: "''"))', "
        strSQLScript = strSQLScript + "intDisplayOrder='\(ptblPOSItemCakeConfigAttribute.intDisplayOrder)' "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "strItemCode='\(ptblPOSItemCakeConfigAttribute.strItemCode)';"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
    }
    
    func ReadCakeAttribute(pstrCakeCode : String, pstrAttributeType : String) -> [String] {
        var strAttributeList : [String] = []
        switch(pstrAttributeType)
        {
        case "Print":
            strSQLScript = "SELECT  "
            strSQLScript = strSQLScript + "strCakeAttributeDescPrint "
            strSQLScript = strSQLScript + "FROM "
            strSQLScript = strSQLScript + "tblPOSItemCakeConfigAttribute "
            strSQLScript = strSQLScript + "WHERE "
            strSQLScript = strSQLScript + "strCakeCode='\(pstrCakeCode)' "
            strSQLScript = strSQLScript + "AND strCakeAttributeDescPrint<>'' "
            strSQLScript = strSQLScript + "GROUP BY "
            strSQLScript = strSQLScript + "strCakeAttributeDescPrint "
            strSQLScript = strSQLScript + "ORDER BY "
            strSQLScript = strSQLScript + "MIN(intDisplayOrder) "
            break
        case "Flavor":
            strSQLScript = "SELECT  "
            strSQLScript = strSQLScript + "strCakeAttributeDescFlavor "
            strSQLScript = strSQLScript + "FROM "
            strSQLScript = strSQLScript + "tblPOSItemCakeConfigAttribute "
            strSQLScript = strSQLScript + "WHERE "
            strSQLScript = strSQLScript + "strCakeCode='\(pstrCakeCode)' "
            strSQLScript = strSQLScript + "AND strCakeAttributeDescFlavor<>'' "
            strSQLScript = strSQLScript + "GROUP BY "
            strSQLScript = strSQLScript + "strCakeAttributeDescFlavor "
            strSQLScript = strSQLScript + "ORDER BY "
            strSQLScript = strSQLScript + "MIN(intDisplayOrder) "
            break
        case "Size":
            strSQLScript = "SELECT  "
            strSQLScript = strSQLScript + "strCakeAttributeDescSize "
            strSQLScript = strSQLScript + "FROM "
            strSQLScript = strSQLScript + "tblPOSItemCakeConfigAttribute "
            strSQLScript = strSQLScript + "WHERE "
            strSQLScript = strSQLScript + "strCakeCode='\(pstrCakeCode)' "
            strSQLScript = strSQLScript + "AND strCakeAttributeDescSize<>'' "
            strSQLScript = strSQLScript + "GROUP BY "
            strSQLScript = strSQLScript + "strCakeAttributeDescSize "
            strSQLScript = strSQLScript + "ORDER BY "
            strSQLScript = strSQLScript + "MIN(intDisplayOrder) "
            break
        default :
            return strAttributeList
        }
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return strAttributeList}
        
        for tblDataRow : Array in tblDataTable
        {
            strAttributeList.append((tblDataRow[0] ?? "") as! String)
        }
        
        return strAttributeList
    }
    
    func ReadCakeCode(pstrItemCode : String) -> String {
        strSQLScript = "SELECT  "
        strSQLScript = strSQLScript + "strCakeCode "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSItemCakeConfigAttribute "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "strItemCode='\(pstrItemCode)' "
        
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return ""}
        
        for tblDataRow : Array in tblDataTable
        {
            return (tblDataRow[0] ?? "") as! String
        }
        
        return ""
    }
    
    // tblPOSItemImage //
    func UpdatePOSItemImage(ptblPOSItemImage : StructMaster.Table.tblPOSItemImageStructList.tblPOSItemImageStruct) {
        strSQLScript = "INSERT OR IGNORE INTO tblPOSItemImage (strItemCode, strImageURL) "
        strSQLScript = strSQLScript + "VALUES ("
        strSQLScript = strSQLScript + "'\(ptblPOSItemImage.strItemCode)', "
        strSQLScript = strSQLScript + "'\(ptblPOSItemImage.strImageURL)' "
        strSQLScript = strSQLScript + ");"
        do{try self.objConnection.execute(strSQLScript)}catch{objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "SQLite Execute '\(strSQLScript)' Error : \(error)")}
    }
    
    func ReadPOSItemImage() -> StructMaster.Table.tblPOSItemImageStructList {
        var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
        strSQLScript = "SELECT  "
        strSQLScript = strSQLScript + "strItemCode, strImageURL "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSItemImage;"
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return tblPOSItemImageList}
        
        for tblDataRow : Array in tblDataTable
        {
            var tblPOSItemImage : StructMaster.Table.tblPOSItemImageStructList.tblPOSItemImageStruct = StructMaster.Table.tblPOSItemImageStructList.tblPOSItemImageStruct()
            tblPOSItemImage.strItemCode = (tblDataRow[0] ?? "") as! String
            tblPOSItemImage.strImageURL = (tblDataRow[1] ?? "") as! String
            tblPOSItemImageList.Detail.append(tblPOSItemImage)
        }
        
        return tblPOSItemImageList
    }
    
    func ReadPOSItemImage(pstrItemCode : String) -> StructMaster.Table.tblPOSItemImageStructList{
        var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
        strSQLScript = "SELECT  "
        strSQLScript = strSQLScript + "strItemCode, strImageURL "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSItemImage "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "strItemCode='\(pstrItemCode)' "
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return tblPOSItemImageList}
        
        for tblDataRow : Array in tblDataTable
        {
            var tblPOSItemImage : StructMaster.Table.tblPOSItemImageStructList.tblPOSItemImageStruct = StructMaster.Table.tblPOSItemImageStructList.tblPOSItemImageStruct()
            tblPOSItemImage.strItemCode = (tblDataRow[0] ?? "") as! String
            tblPOSItemImage.strImageURL = (tblDataRow[1] ?? "") as! String
            tblPOSItemImageList.Detail.append(tblPOSItemImage)
        }
        
        return tblPOSItemImageList
    }
    
    func ReadPOSItemImage(pstrTypeCode : String) -> StructMaster.Table.tblPOSItemImageStructList{
        var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
        strSQLScript = "SELECT "
        strSQLScript = strSQLScript + "IM.strItemCode, IM.strImageURL "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSItemImage IM "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "IM.strItemCode='\(pstrTypeCode)' "
        strSQLScript = strSQLScript + "UNION "
        strSQLScript = strSQLScript + "SELECT "
        strSQLScript = strSQLScript + "IM.strItemCode, IM.strImageURL "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSItemImage IM LEFT JOIN "
        strSQLScript = strSQLScript + "tblPOSItemTypeConfig IT ON IM.strItemCode = IT.strTypeCode "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "IT.strItemType NOT IN ('Cake') "
        strSQLScript = strSQLScript + "AND IT.strTypeCode='\(pstrTypeCode)' "
        strSQLScript = strSQLScript + "UNION "
        strSQLScript = strSQLScript + "SELECT "
        strSQLScript = strSQLScript + "IM.strItemCode, IM.strImageURL "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSItemImage IM LEFT JOIN "
        strSQLScript = strSQLScript + "tblPOSItemTypeConfig IT ON IM.strItemCode = IT.strTypeCode INNER JOIN "
        strSQLScript = strSQLScript + "tblPOSItemCakeConfigAttribute IC ON IT.strTypeCode = IC.strCakeCode "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "IT.strItemType = 'Cake' "
        strSQLScript = strSQLScript + "AND IC.strItemCode='\(pstrTypeCode)'; "
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return tblPOSItemImageList}
        
        for tblDataRow : Array in tblDataTable
        {
            var tblPOSItemImage : StructMaster.Table.tblPOSItemImageStructList.tblPOSItemImageStruct = StructMaster.Table.tblPOSItemImageStructList.tblPOSItemImageStruct()
            tblPOSItemImage.strItemCode = (tblDataRow[0] ?? "") as! String
            tblPOSItemImage.strImageURL = (tblDataRow[1] ?? "") as! String
            tblPOSItemImageList.Detail.append(tblPOSItemImage)
        }
        
        return tblPOSItemImageList
    }
    
    // tblPreOrderSalesHeader //
    // tblPreOrderSalesDetail //
    // tblPreOrderDeliveryHeader //
    // tblPreOrderDeliveryDetail //
    
    // PreOrder //
    // PreOrderItem //
    func ReadPreOrderItem(pstrItemType : String) -> StructMaster.PreOrder.PreOrderItemStructList {
        var PreOrderItemList : StructMaster.PreOrder.PreOrderItemStructList = StructMaster.PreOrder.PreOrderItemStructList()
        switch(pstrItemType) {
        case "Cake":
            strSQLScript = "SELECT "
            strSQLScript = strSQLScript + "IH.strItemType, IH.strTypeCode, IH.strTypeDesc, IH.strtypeRemark, IH.intAddOnCount, IH.intDisplayOrder, "
            strSQLScript = strSQLScript + "MIN(ID.decSalesAmt) AS decSalesAmtFm, "
            strSQLScript = strSQLScript + "MAX(ID.decSalesAmt) AS decSalesAmtTo, "
            strSQLScript = strSQLScript + "MIN(ID.intDeliveryDay) AS intDeliveryDayFm, "
            strSQLScript = strSQLScript + "MAX(ID.intDeliveryDay) AS intDeliveryDayTo "
            strSQLScript = strSQLScript + "FROM "
            strSQLScript = strSQLScript + "tblPOSItemTypeConfig IH INNER JOIN "
            strSQLScript = strSQLScript + "tblPOSItemCakeConfigAttribute IC ON IH.strTypeCode = IC.strCakeCode INNER JOIN "
            strSQLScript = strSQLScript + "tblPOSItemMaster ID ON IC.strItemCode = ID.strItemCode "
            strSQLScript = strSQLScript + "WHERE "
            strSQLScript = strSQLScript + "IH.strItemType ='Cake' "
            strSQLScript = strSQLScript + "GROUP BY "
            strSQLScript = strSQLScript + "IH.strItemType, IH.strTypeCode, IH.strTypeDesc, IH.strtypeRemark, IH.intAddOnCount, IH.intDisplayOrder;"
            break
        case "Pastrie", "Canape", "Festive", "Lollipop", "Topper", "Delivery":
            strSQLScript = "SELECT "
            strSQLScript = strSQLScript + "IH.strItemType, IH.strTypeCode, IH.strTypeDesc, IH.strtypeRemark, IH.intAddOnCount, IH.intDisplayOrder, "
            strSQLScript = strSQLScript + "MIN(ID.decSalesAmt) AS decSalesAmtFm, "
            strSQLScript = strSQLScript + "MAX(ID.decSalesAmt) AS decSalesAmtTo, "
            strSQLScript = strSQLScript + "MIN(ID.intDeliveryDay) AS intDeliveryDayFm, "
            strSQLScript = strSQLScript + "MAX(ID.intDeliveryDay) AS intDeliveryDayTo "
            strSQLScript = strSQLScript + "FROM "
            strSQLScript = strSQLScript + "tblPOSItemTypeConfig IH INNER JOIN "
            strSQLScript = strSQLScript + "tblPOSItemMaster ID ON IH.strTypeCode = ID.strItemCode "
            strSQLScript = strSQLScript + "WHERE "
            strSQLScript = strSQLScript + "IH.strItemType ='\(pstrItemType)' "
            strSQLScript = strSQLScript + "GROUP BY "
            strSQLScript = strSQLScript + "IH.strItemType, IH.strTypeCode, IH.strTypeDesc, IH.strtypeRemark, IH.intAddOnCount, IH.intDisplayOrder;"
            break
        default:
            return PreOrderItemList
        }
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return PreOrderItemList}
        
        for tblDataRow : Array in tblDataTable
        {
            var PreOrderItem : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct = StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct()
            PreOrderItem.strItemType = (tblDataRow[0] ?? "") as! String
            PreOrderItem.strTypeCode = (tblDataRow[1] ?? "") as! String
            PreOrderItem.strTypeDesc = (tblDataRow[2] ?? "") as! String
            PreOrderItem.strTypeRemark = (tblDataRow[3] ?? "") as! String
            PreOrderItem.intAddOnCount = Int64((tblDataRow[4] ?? 0) as! Int64)
            PreOrderItem.intDisplayOrder = Int64((tblDataRow[5] ?? 0) as! Int64)
            PreOrderItem.decSalesAmtFm = Double((tblDataRow[6] ?? 0.0) as! Double)
            PreOrderItem.decSalesAmtTo = Double((tblDataRow[7] ?? 0.0) as! Double)
            PreOrderItem.intDeliveryDayFm = Int64((tblDataRow[8] ?? 0) as! Int64)
            PreOrderItem.intDeliveryDayFm = Int64((tblDataRow[9] ?? 0) as! Int64)
            
            PreOrderItemList.Detail.append(PreOrderItem)
        }
        
        
        return PreOrderItemList
    }

    func ReadPreOrderItem(pstrItemType : String, pstrTypeCode : String) -> StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct {
        var PreOrderItem : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct = StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct()
        switch(pstrItemType) {
        case "Pastrie", "Canape", "Festive", "Lollipop", "Topper", "Delivery":
            strSQLScript = "SELECT "
            strSQLScript = strSQLScript + "IH.strItemType, IH.strTypeCode, IH.strTypeDesc, IH.strtypeRemark, IH.intAddOnCount, IH.intDisplayOrder, "
            strSQLScript = strSQLScript + "MIN(ID.decSalesAmt) AS decSalesAmtFm, "
            strSQLScript = strSQLScript + "MAX(ID.decSalesAmt) AS decSalesAmtTo, "
            strSQLScript = strSQLScript + "MIN(ID.intDeliveryDay) AS intDeliveryDayFm, "
            strSQLScript = strSQLScript + "MAX(ID.intDeliveryDay) AS intDeliveryDayTo "
            strSQLScript = strSQLScript + "FROM "
            strSQLScript = strSQLScript + "tblPOSItemTypeConfig IH INNER JOIN "
            strSQLScript = strSQLScript + "tblPOSItemMaster ID ON IH.strTypeCode = ID.strItemCode "
            strSQLScript = strSQLScript + "WHERE "
            strSQLScript = strSQLScript + "IH.strItemType ='\(pstrItemType)' "
            strSQLScript = strSQLScript + "AND IH.strTypeCode ='\(pstrTypeCode)' "
            strSQLScript = strSQLScript + "GROUP BY "
            strSQLScript = strSQLScript + "IH.strItemType, IH.strTypeCode, IH.strTypeDesc, IH.strtypeRemark, IH.intAddOnCount, IH.intDisplayOrder;"
            break
        default:
            return PreOrderItem
        }
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return PreOrderItem}
        
        for tblDataRow : Array in tblDataTable
        {
            PreOrderItem.strItemType = (tblDataRow[0] ?? "") as! String
            PreOrderItem.strTypeCode = (tblDataRow[1] ?? "") as! String
            PreOrderItem.strTypeDesc = (tblDataRow[2] ?? "") as! String
            PreOrderItem.strTypeRemark = (tblDataRow[3] ?? "") as! String
            PreOrderItem.intAddOnCount = Int64((tblDataRow[4] ?? 0) as! Int64)
            PreOrderItem.intDisplayOrder = Int64((tblDataRow[5] ?? 0) as! Int64)
            PreOrderItem.decSalesAmtFm = Double((tblDataRow[6] ?? 0.0) as! Double)
            PreOrderItem.decSalesAmtTo = Double((tblDataRow[7] ?? 0.0) as! Double)
            PreOrderItem.intDeliveryDayFm = Int64((tblDataRow[8] ?? 0) as! Int64)
            PreOrderItem.intDeliveryDayFm = Int64((tblDataRow[9] ?? 0) as! Int64)
        }
        
        return PreOrderItem
    }

    func ReadPreOrderItemCake() -> StructMaster.PreOrder.PreOrderItemCakeStructList {
        var PreOrderItemCakeList : StructMaster.PreOrder.PreOrderItemCakeStructList = StructMaster.PreOrder.PreOrderItemCakeStructList()
        strSQLScript = "SELECT "
        strSQLScript = strSQLScript + "IH.strItemType, IH.strTypeCode, IH.strTypeDesc, IH.strtypeRemark, IH.intAddOnCount, IH.intDisplayOrder, "
        strSQLScript = strSQLScript + "COUNT(IC.strItemCode) AS intDetailCount, "
        strSQLScript = strSQLScript + "MIN(ID.decSalesAmt) AS decSalesAmtFm, "
        strSQLScript = strSQLScript + "MAX(ID.decSalesAmt) AS decSalesAmtTo, "
        strSQLScript = strSQLScript + "MIN(ID.intDeliveryDay) AS intDeliveryDayFm, "
        strSQLScript = strSQLScript + "MAX(ID.intDeliveryDay) AS intDeliveryDayTo "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSItemTypeConfig IH INNER JOIN "
        strSQLScript = strSQLScript + "tblPOSItemCakeConfigAttribute IC ON IH.strTypeCode = IC.strCakeCode INNER JOIN "
        strSQLScript = strSQLScript + "tblPOSItemMaster ID ON IC.strItemCode = ID.strItemCode "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "IH.strItemType ='Cake' "
        strSQLScript = strSQLScript + "GROUP BY "
        strSQLScript = strSQLScript + "IH.strItemType, IH.strTypeCode, IH.strTypeDesc, IH.strtypeRemark, IH.intAddOnCount, IH.intDisplayOrder;"
        
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return PreOrderItemCakeList}
        
        for tblDataRow : Array in tblDataTable
        {
            var PreOrderItemCake : StructMaster.PreOrder.PreOrderItemCakeStructList.PreOrderItemCakeStruct = StructMaster.PreOrder.PreOrderItemCakeStructList.PreOrderItemCakeStruct()
            PreOrderItemCake.strItemType = (tblDataRow[0] ?? "") as! String
            PreOrderItemCake.strTypeCode = (tblDataRow[1] ?? "") as! String
            PreOrderItemCake.strTypeDesc = (tblDataRow[2] ?? "") as! String
            PreOrderItemCake.strTypeRemark = (tblDataRow[3] ?? "") as! String
            PreOrderItemCake.intAddOnCount = Int64((tblDataRow[4] ?? 0) as! Int64)
            PreOrderItemCake.intDisplayOrder = Int64((tblDataRow[5] ?? 0) as! Int64)
            PreOrderItemCake.decSalesAmtFm = Double((tblDataRow[6] ?? 0.0) as! Double)
            PreOrderItemCake.decSalesAmtTo = Double((tblDataRow[7] ?? 0.0) as! Double)
            PreOrderItemCake.intDeliveryDayFm = Int64((tblDataRow[8] ?? 0) as! Int64)
            PreOrderItemCake.intDeliveryDayFm = Int64((tblDataRow[9] ?? 0) as! Int64)
            
            PreOrderItemCakeList.Detail.append(PreOrderItemCake)
        }
        
        return PreOrderItemCakeList
    }
    
    func ReadPreOrderItemCake(pstrTypeCode : String) -> StructMaster.PreOrder.PreOrderItemCakeStructList.PreOrderItemCakeStruct {
        var PreOrderItemCake : StructMaster.PreOrder.PreOrderItemCakeStructList.PreOrderItemCakeStruct = StructMaster.PreOrder.PreOrderItemCakeStructList.PreOrderItemCakeStruct()
        strSQLScript = "SELECT "
        strSQLScript = strSQLScript + "IH.strItemType, IH.strTypeCode, IH.strTypeDesc, IH.strtypeRemark, COUNT(IC.strItemCode) AS intDetailCount, IH.intDisplayOrder, "
        strSQLScript = strSQLScript + "MIN(ID.decSalesAmt) AS decSalesAmtFm, "
        strSQLScript = strSQLScript + "MAX(ID.decSalesAmt) AS decSalesAmtTo, "
        strSQLScript = strSQLScript + "MIN(ID.intDeliveryDay) AS intDeliveryDayFm, "
        strSQLScript = strSQLScript + "MAX(ID.intDeliveryDay) AS intDeliveryDayTo "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSItemTypeConfig IH INNER JOIN "
        strSQLScript = strSQLScript + "tblPOSItemCakeConfigAttribute IC ON IH.strTypeCode = IC.strCakeCode INNER JOIN "
        strSQLScript = strSQLScript + "tblPOSItemMaster ID ON IC.strItemCode = ID.strItemCode "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "IH.strItemType ='Cake' "
        strSQLScript = strSQLScript + "AND IH.strTypeCode ='\(pstrTypeCode)' "
        strSQLScript = strSQLScript + "GROUP BY "
        strSQLScript = strSQLScript + "IH.strItemType, IH.strTypeCode, IH.strTypeDesc, IH.strtypeRemark, IH.intAddOnCount, IH.intDisplayOrder;"
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return PreOrderItemCake}
        
        for tblDataRow : Array in tblDataTable
        {
            PreOrderItemCake.strItemType = (tblDataRow[0] ?? "") as! String
            PreOrderItemCake.strTypeCode = (tblDataRow[1] ?? "") as! String
            PreOrderItemCake.strTypeDesc = (tblDataRow[2] ?? "") as! String
            PreOrderItemCake.strTypeRemark = (tblDataRow[3] ?? "") as! String
            PreOrderItemCake.intDetailCount = Int64((tblDataRow[4] ?? 0) as! Int64)
            PreOrderItemCake.intDisplayOrder = Int64((tblDataRow[5] ?? 0) as! Int64)
            PreOrderItemCake.decSalesAmtFm = Double((tblDataRow[6] ?? 0.0) as! Double)
            PreOrderItemCake.decSalesAmtTo = Double((tblDataRow[7] ?? 0.0) as! Double)
            PreOrderItemCake.intDeliveryDayFm = Int64((tblDataRow[8] ?? 0) as! Int64)
            PreOrderItemCake.intDeliveryDayFm = Int64((tblDataRow[9] ?? 0) as! Int64)
        }
        
        return PreOrderItemCake
    }

    func ReadPreOrderItemCakeDetail(pstrCakeCode : String, pstrAttributePrint : String, pstrAttributeFlavor : String, pstrAttributeSize : String) -> StructMaster.PreOrder.PreOrderItemCakeStructList.PreOrderItemCakeStruct {
        var PreOrderItemCake : StructMaster.PreOrder.PreOrderItemCakeStructList.PreOrderItemCakeStruct = StructMaster.PreOrder.PreOrderItemCakeStructList.PreOrderItemCakeStruct()
        strSQLScript = "SELECT "
        strSQLScript = strSQLScript + "IH.strItemType, IH.strTypeCode, IH.strTypeDesc, IH.strtypeRemark, IH.intDisplayOrder, "
        strSQLScript = strSQLScript + "COUNT(IC.strItemCode) AS intDetailCount, "
        strSQLScript = strSQLScript + "MIN(ID.decSalesAmt) AS decSalesAmtFm, "
        strSQLScript = strSQLScript + "MAX(ID.decSalesAmt) AS decSalesAmtTo, "
        strSQLScript = strSQLScript + "MIN(ID.intDeliveryDay) AS intDeliveryDayFm, "
        strSQLScript = strSQLScript + "MAX(ID.intDeliveryDay) AS intDeliveryDayTo "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSItemTypeConfig IH INNER JOIN "
        strSQLScript = strSQLScript + "tblPOSItemCakeConfigAttribute IC ON IH.strTypeCode = IC.strCakeCode INNER JOIN "
        strSQLScript = strSQLScript + "tblPOSItemMaster ID ON IC.strItemCode = ID.strItemCode "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "IH.strItemType ='Cake' "
        strSQLScript = strSQLScript + "AND IH.strTypeCode ='\(pstrCakeCode)' "
        strSQLScript = strSQLScript + "AND (IC.strCakeAttributeDescPrint='\(pstrAttributePrint)' OR IC.strCakeAttributeDescPrint='' OR ''='\(pstrAttributePrint)') "
        strSQLScript = strSQLScript + "AND (IC.strCakeAttributeDescFlavor='\(pstrAttributeFlavor)' OR IC.strCakeAttributeDescFlavor='' OR ''='\(pstrAttributeFlavor)') "
        strSQLScript = strSQLScript + "AND (IC.strCakeAttributeDescSize='\(pstrAttributeSize)' OR IC.strCakeAttributeDescSize='' OR ''='\(pstrAttributeSize)') "
        strSQLScript = strSQLScript + "GROUP BY "
        strSQLScript = strSQLScript + "IH.strItemType, IH.strTypeCode, IH.strTypeDesc, IH.strtypeRemark, IH.intAddOnCount, IH.intDisplayOrder;"
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return PreOrderItemCake}
        strSQLScript = "SELECT "
        strSQLScript = strSQLScript + "ID.strItemCode, ID.strItemDesc, ID.strItemRemark, ID.decOriginAmt, ID.decSalesAmt, "
        strSQLScript = strSQLScript + "IC.strCakeAttributeDescPrint, IC.strCakeAttributeDescFlavor, IC.strCakeAttributeDescSize, IC.intDisplayOrder "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSItemTypeConfig IH INNER JOIN "
        strSQLScript = strSQLScript + "tblPOSItemCakeConfigAttribute IC ON IH.strTypeCode = IC.strCakeCode INNER JOIN "
        strSQLScript = strSQLScript + "tblPOSItemMaster ID ON IC.strItemCode = ID.strItemCode "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "IH.strItemType ='Cake' "
        strSQLScript = strSQLScript + "AND IH.strTypeCode ='\(pstrCakeCode)' "
        strSQLScript = strSQLScript + "AND (IC.strCakeAttributeDescPrint='\(pstrAttributePrint)' OR IC.strCakeAttributeDescPrint='' OR ''='\(pstrAttributePrint)') "
        strSQLScript = strSQLScript + "AND (IC.strCakeAttributeDescFlavor='\(pstrAttributeFlavor)' OR IC.strCakeAttributeDescFlavor='' OR ''='\(pstrAttributeFlavor)') "
        strSQLScript = strSQLScript + "AND (IC.strCakeAttributeDescSize='\(pstrAttributeSize)' OR IC.strCakeAttributeDescSize='' OR ''='\(pstrAttributeSize)') "
        guard let tblDataDetailTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return PreOrderItemCake}
        
        for tblDataRow : Array in tblDataTable
        {
            PreOrderItemCake.strItemType = (tblDataRow[0] ?? "") as! String
            PreOrderItemCake.strTypeCode = (tblDataRow[1] ?? "") as! String
            PreOrderItemCake.strTypeDesc = (tblDataRow[2] ?? "") as! String
            PreOrderItemCake.strTypeRemark = (tblDataRow[3] ?? "") as! String
            PreOrderItemCake.intDisplayOrder = Int64((tblDataRow[4] ?? 0) as! Int64)
            PreOrderItemCake.intDetailCount = Int64((tblDataRow[5] ?? 0) as! Int64)
            PreOrderItemCake.decSalesAmtFm = Double((tblDataRow[6] ?? 0.0) as! Double)
            PreOrderItemCake.decSalesAmtTo = Double((tblDataRow[7] ?? 0.0) as! Double)
            PreOrderItemCake.intDeliveryDayFm = Int64((tblDataRow[8] ?? 0) as! Int64)
            PreOrderItemCake.intDeliveryDayFm = Int64((tblDataRow[9] ?? 0) as! Int64)
            for tblDataDetailRow : Array in tblDataDetailTable
            {
                var CakeItem : StructMaster.PreOrder.PreOrderItemCakeStructList.PreOrderItemCakeStruct.PreOrderItemCakeDetailStruct = StructMaster.PreOrder.PreOrderItemCakeStructList.PreOrderItemCakeStruct.PreOrderItemCakeDetailStruct()
                CakeItem.strItemCode = (tblDataDetailRow[0] ?? "") as! String
                CakeItem.strItemDesc = (tblDataDetailRow[1] ?? "") as! String
                CakeItem.strItemRemark = (tblDataDetailRow[2] ?? "") as! String
                CakeItem.decOriginAmt = Double((tblDataDetailRow[3] ?? 0.0) as! Double)
                CakeItem.decSalesAmt = Double((tblDataDetailRow[4] ?? 0.0) as! Double)
                CakeItem.strCakeAttributeDescPrint = (tblDataDetailRow[5] ?? "") as! String
                CakeItem.strCakeAttributeDescFlavor = (tblDataDetailRow[6] ?? "") as! String
                CakeItem.strCakeAttributeDescSize = (tblDataDetailRow[7] ?? "") as! String
                CakeItem.intDisplayOrder = Int64((tblDataDetailRow[8] ?? 0) as! Int64)
                PreOrderItemCake.CakeItemList.append(CakeItem)
            }
        }
        return PreOrderItemCake
    }
    
    // PreOrederLollipopMessage //
    func ReadPreOrderLollipopMessage(pstrParentID : String) -> [StructMaster.PreOrder.PreOrderItemCakeMessageStructList.PreOrderItemCakeMessageStruct] {
        var PreOrderItemCakeMessageList : [StructMaster.PreOrder.PreOrderItemCakeMessageStructList.PreOrderItemCakeMessageStruct] = []
        strSQLScript = "SELECT "
        strSQLScript = strSQLScript + "IH.strMessageID, IH.strMessageDesc, IH.intDisplayOrder, "
        strSQLScript = strSQLScript + "COUNT(ID.strMessageDesc) AS intDetailCount "
        strSQLScript = strSQLScript + "FROM "
        strSQLScript = strSQLScript + "tblPOSLollipopMessage IH LEFT JOIN "
        strSQLScript = strSQLScript + "tblPOSLollipopMessage ID ON IH.strMessageID = ID.strParentID "
        strSQLScript = strSQLScript + "WHERE "
        strSQLScript = strSQLScript + "IH.strParentID ='\(pstrParentID)' "
        strSQLScript = strSQLScript + "GROUP BY "
        strSQLScript = strSQLScript + "IH.strMessageID, IH.strMessageDesc, IH.intDisplayOrder "
        strSQLScript = strSQLScript + "ORDER BY "
        strSQLScript = strSQLScript + "IH.intDisplayOrder;"
        guard let tblDataTable : Statement = try? self.objConnection.prepare(strSQLScript) else { return PreOrderItemCakeMessageList}
        
        for tblDataRow : Array in tblDataTable
        {
            var PreOrderItemCakeMessage : StructMaster.PreOrder.PreOrderItemCakeMessageStructList.PreOrderItemCakeMessageStruct = StructMaster.PreOrder.PreOrderItemCakeMessageStructList.PreOrderItemCakeMessageStruct()
            PreOrderItemCakeMessage.strMessageID = (tblDataRow[0] ?? "") as! String
            PreOrderItemCakeMessage.strMessageDesc = (tblDataRow[1] ?? "") as! String
            PreOrderItemCakeMessage.intDisplayOrder = (tblDataRow[2] ?? 0) as! Int64
            PreOrderItemCakeMessage.intChildCount = (tblDataRow[3] ?? 0) as! Int64
            PreOrderItemCakeMessageList.append(PreOrderItemCakeMessage)
        }
        
        return PreOrderItemCakeMessageList
    }
    
    // Private Function //
    struct SQLiteTableColumn {
        var intColumnID : Int = 0
        var strColumnName : String = ""
        var strColumnType : String = ""
    }
    
    private func BindingBool(objBinding : Binding) -> Bool {
        if (objBinding as! String == "true")
        {
            return true
        }
        else if (objBinding as! String == "false")
        {
            return false
        }
        else if objBinding as! Int64 == 1
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    private func BindingDate(objBinding : Binding) -> Date {
        let objDateFormatter = DateFormatter()
        objDateFormatter.dateFormat = "yyyy/MM/dd"
        
        if (objBinding as! String == "")
        {
            return Date()
        }
        
        return objDateFormatter.date(from: objBinding as! String)!
    }
    
    private func BindingTime(objBinding : Binding) -> Date {
        let objTimeFormatter = DateFormatter()
        objTimeFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        
        if (objBinding as! String == "")
        {
            return Date()
        }
        
        return objTimeFormatter.date(from: objBinding as! String)!
    }
    
    private func GetBoolString(bitBool : Bool) -> String {
        if bitBool
        {
            return "true"
        }
        else
        {
            return "false"
        }
    }
    
    private func GetDateString(pdtmDate : Date) -> String {
        let objDateFormatter : DateFormatter = DateFormatter()
        objDateFormatter.dateFormat = "yyyy/MM/dd"
        
        return objDateFormatter.string(from : pdtmDate)
    }
    
    private func GetTimeString(pdtmTime : Date) -> String {
        let objDateFormatter : DateFormatter = DateFormatter()
        objDateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        
        return objDateFormatter.string(from : pdtmTime)
    }
}
