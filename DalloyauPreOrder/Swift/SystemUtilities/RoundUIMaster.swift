
import UIKit

@IBDesignable public class RoundUIView: UIView {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var IsShadow : Bool = false{
        didSet
        {
            self.layer.masksToBounds = IsShadow
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 0.5
            self.layer.shadowOffset = CGSize(width: -1, height: 1)
            self.layer.shadowRadius = 1
        }
    }
}

@IBDesignable public class RoundUIButton: UIButton {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}

@IBDesignable public class RoundUILabel: UILabel {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}

@IBDesignable public class RoundUITextField : UITextField {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}

@IBDesignable public class RoundUITextView : UITextView {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}

@IBDesignable public class RoundUIImageView : UIImageView {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}

@IBDesignable public class RoundUICollectionView : UICollectionView {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}

@IBDesignable public class RoundUICollectionViewCell : UICollectionViewCell {
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}

extension UIViewController {
    
    func GetDateString(pdtmDate : Date) -> String {
        let objDateFormatter : DateFormatter = DateFormatter()
        objDateFormatter.dateFormat = "yyyy/MM/dd"
        
        return objDateFormatter.string(from : pdtmDate)
    }
    
    func GetTimeString(pdtmTime : Date) -> String {
        let objDateFormatter : DateFormatter = DateFormatter()
        objDateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        
        return objDateFormatter.string(from : pdtmTime)
    }
    
    func GetQtyString(pQty : Int64) -> String {
        let objFormatter : NumberFormatter = NumberFormatter()
        objFormatter.numberStyle = .currency
        objFormatter.currencySymbol = ""
        objFormatter.maximumFractionDigits = 0
        objFormatter.minimumFractionDigits = 0
        
        if (pQty != 0)
        {
            return "\(objFormatter.string(for : pQty)!)"
        }
        else
        {
            return ""
        }
    }
    
    func GetQtyString(pQty : Double) -> String {
        let objFormatter : NumberFormatter = NumberFormatter()
        objFormatter.numberStyle = .currency
        objFormatter.currencySymbol = ""
        objFormatter.maximumFractionDigits = 0
        objFormatter.minimumFractionDigits = 0
        
        if (pQty != 0.0)
        {
            return "\(objFormatter.string(for : pQty)!)"
        }
        else
        {
            return ""
        }
    }
    
    func GetAmtString(pAmt : Double) -> String {
        let objFormatter : NumberFormatter = NumberFormatter()
        objFormatter.numberStyle = .currency
        objFormatter.currencySymbol = ""
        objFormatter.maximumFractionDigits = 0
        objFormatter.minimumFractionDigits = 0
        
        if (pAmt != 0.0)
        {
            return "HK$ \(objFormatter.string(for : pAmt)!)"
        }
        else
        {
            return ""
        }
    }
    
    func GetAmtRangeString(pAmtFm : Double, pAmtTo : Double) -> String {
        if (pAmtFm != pAmtTo)
        {
            return "\(GetAmtString(pAmt : pAmtFm)) and up"
        }
        else
        {
            return "\(GetAmtString(pAmt : pAmtFm))"
        }
    }
    
    func GetDeliveryDateString(pintDeliveryDay : Int64) -> String {
        let objDateFormatter : DateFormatter = DateFormatter()
        var dateComponent = DateComponents()
        var dtmDeliveryDay : Date = Date()
        var strRetrunString : String = ""
        objDateFormatter.dateFormat = "dd/M"
        dateComponent.day = Int(pintDeliveryDay)
        dateComponent.day = dateComponent.day! + (Calendar.current.component(.day, from: dtmDeliveryDay))
        dtmDeliveryDay = Calendar.current.date(byAdding: dateComponent, to: dtmDeliveryDay)!
        
        strRetrunString = "\(objDateFormatter.string(from : dtmDeliveryDay)) (\(pintDeliveryDay) day)"
        
        return strRetrunString
    }
}

extension UICollectionViewCell {
    
    func GetDateString(pdtmDate : Date) -> String {
        let objDateFormatter : DateFormatter = DateFormatter()
        objDateFormatter.dateFormat = "yyyy/MM/dd"
        
        return objDateFormatter.string(from : pdtmDate)
    }
    
    func GetTimeString(pdtmTime : Date) -> String {
        let objDateFormatter : DateFormatter = DateFormatter()
        objDateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        
        return objDateFormatter.string(from : pdtmTime)
    }
    
    func GetQtyString(pQty : Int64) -> String {
        let objFormatter : NumberFormatter = NumberFormatter()
        objFormatter.numberStyle = .currency
        objFormatter.currencySymbol = ""
        objFormatter.maximumFractionDigits = 0
        objFormatter.minimumFractionDigits = 0
        
        if (pQty != 0)
        {
            return "\(objFormatter.string(for : pQty)!)"
        }
        else
        {
            return ""
        }
    }
    
    func GetQtyString(pQty : Double) -> String {
        let objFormatter : NumberFormatter = NumberFormatter()
        objFormatter.numberStyle = .currency
        objFormatter.currencySymbol = ""
        objFormatter.maximumFractionDigits = 0
        objFormatter.minimumFractionDigits = 0
        
        if (pQty != 0.0)
        {
            return "\(objFormatter.string(for : pQty)!)"
        }
        else
        {
            return ""
        }
    }
    
    func GetAmtString(pAmt : Double) -> String {
        let objFormatter : NumberFormatter = NumberFormatter()
        objFormatter.numberStyle = .currency
        objFormatter.currencySymbol = ""
        objFormatter.maximumFractionDigits = 0
        objFormatter.minimumFractionDigits = 0
        
        if (pAmt != 0.0)
        {
            return "HK$ \(objFormatter.string(for : pAmt)!)"
        }
        else
        {
            return ""
        }
    }
    
    func GetAmtRangeString(pAmtFm : Double, pAmtTo : Double) -> String {
        if (pAmtFm != pAmtTo)
        {
            return "\(GetAmtString(pAmt : pAmtFm)) and up"
        }
        else
        {
            return "\(GetAmtString(pAmt : pAmtFm))"
        }
    }
    
    func GetDeliveryDateString(pintDeliveryDay : Int64) -> String {
        let objDateFormatter : DateFormatter = DateFormatter()
        var dateComponent = DateComponents()
        var dtmDeliveryDay : Date = Date()
        var strRetrunString : String = ""
        objDateFormatter.dateFormat = "dd/M"
        dateComponent.day = Int(pintDeliveryDay)
        dateComponent.day = dateComponent.day! + (Calendar.current.component(.day, from: dtmDeliveryDay))
        dtmDeliveryDay = Calendar.current.date(byAdding: dateComponent, to: dtmDeliveryDay)!
        
        strRetrunString = "\(objDateFormatter.string(from : dtmDeliveryDay)) (\(pintDeliveryDay) day)"
        
        return strRetrunString
    }
}

extension UICollectionView {
    
    func DeselectAll() {
        self.indexPathsForVisibleItems.forEach {
            self.deselectItem(at: $0, animated: true)
        }
    }
}
