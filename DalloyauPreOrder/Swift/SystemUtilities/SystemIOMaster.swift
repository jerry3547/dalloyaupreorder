//
//  SystemIOMaster.swift
//  DalloyauPreOrder
//
//  Created by jerry on 26/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class SystemIOMaster
{
    func CheckFilesSize(pstrFilePath : String) -> String {
        if (FileManager.default.fileExists(atPath : pstrFilePath) == true)
        {
            let objAttributes = try! FileManager.default.attributesOfItem(atPath: pstrFilePath)
            let intFileSize = objAttributes[FileAttributeKey.size] as! UInt64
            
            let objFormatter : NumberFormatter = NumberFormatter()
            objFormatter.numberStyle = .currency
            objFormatter.currencySymbol = ""
            objFormatter.maximumFractionDigits = 2
            objFormatter.minimumFractionDigits = 2
            
            if (intFileSize != 0)
            {
                return "\(objFormatter.string(for : intFileSize / 1000)!) KB"
            }
        }
        return ""
    }
    
    class SystemLog {
        func WriteLogFile(pstrClassName : String, pstrFunctionName : String,  pstrLogMessage : String) {
            let objDateFormatter : DateFormatter = DateFormatter()
            let objTimeFormatter : DateFormatter = DateFormatter()
            objDateFormatter.dateFormat = "yyyyMMdd"
            objTimeFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
            
            let strLogMessage : String = "\(objTimeFormatter.string(from: Date())) [\(URL(fileURLWithPath : pstrClassName).lastPathComponent)-\(pstrFunctionName)] \(pstrLogMessage)"
            let strLogFilePath : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/SystemLog/"
            let strLogFileName : String = "\(objDateFormatter.string(from: Date())).log"
            let urlLogFilePath : URL = URL(fileURLWithPath: strLogFilePath + strLogFileName)
            
            // Check Directories exist or not //
            if (FileManager.default.fileExists(atPath: strLogFilePath) != true)
            {
                try! FileManager.default.createDirectory(atPath: strLogFilePath, withIntermediateDirectories: true)
            }
            
            print("\(pstrFunctionName) - \(pstrLogMessage)")
            do
            {
                let objFileHandle = try FileHandle(forWritingTo: urlLogFilePath)
                objFileHandle.seekToEndOfFile()
                objFileHandle.write(strLogMessage.data(using: .utf8)!)
                objFileHandle.write("\n".data(using: .utf8)!)
                objFileHandle.closeFile()
            }
            catch
            {
                try! strLogMessage.data(using: .utf8)?.write(to: urlLogFilePath)
            }
        }
        
        func ReadLogFile(pstrFilePath : String) -> String {
            let strLogFilePath : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/SystemLog/"
            let urlLogFilePath : URL = URL(fileURLWithPath: strLogFilePath + pstrFilePath)
            let strLogFileData : String = try! String(contentsOf: urlLogFilePath, encoding: .utf8)
            
            return strLogFileData
        }
    }
    
    class SystemImage {
        public enum ImageType {
            case Item
            case Store
        }
        
        func DownloadItemImageFiles() {
            var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
            tblPOSItemImageList.Read()
            
            // Check Directory Exist
            let strDestinationDirectory : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/ItemImages"
            if (FileManager.default.fileExists(atPath: strDestinationDirectory) != true) {
                do{
                    try FileManager.default.createDirectory(atPath: strDestinationDirectory, withIntermediateDirectories: true, attributes: nil)
                }catch{
                    let objSystemLog : SystemIOMaster.SystemLog = SystemIOMaster.SystemLog()
                    objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "Cannot create directory : \(strDestinationDirectory)")
                }
            }
            
            for tblPOSItemImage : StructMaster.Table.tblPOSItemImageStructList.tblPOSItemImageStruct in tblPOSItemImageList.Detail {
                DownloadImageFile(pstrImageType : "ItemImages", pstrImageURL : tblPOSItemImage.strImageURL)
            }
        }
        
        func DownloadStoreImageFiles() {
            var tblPOSStoreList : StructMaster.Table.tblPOSStoreStructList = StructMaster.Table.tblPOSStoreStructList()
            tblPOSStoreList.Read()
            
            // Check Directory Exist
            let strDestinationDirectory : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/StoreImages"
            if (FileManager.default.fileExists(atPath: strDestinationDirectory) != true) {
                do{
                    try FileManager.default.createDirectory(atPath: strDestinationDirectory, withIntermediateDirectories: true, attributes: nil)
                }catch{
                    let objSystemLog : SystemIOMaster.SystemLog = SystemIOMaster.SystemLog()
                    objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "Cannot create directory : \(strDestinationDirectory)")
                }
            }
            
            for tblPOSStore : StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct in tblPOSStoreList.Detail {
                DownloadImageFile(pstrImageType : "StoreImages", pstrImageURL : tblPOSStore.strStoreImageURL)
            }
        }
        
        private func DownloadImageFile(pstrImageType : String, pstrImageURL : String) {
            let urlImageURL = URL(string: pstrImageURL)
            let strDestinationDirectory : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/" + pstrImageType
            let strDestinationPath : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/" + pstrImageType + "/" + urlImageURL!.lastPathComponent
            let objSystemLog : SystemIOMaster.SystemLog = SystemIOMaster.SystemLog()
            
            do
            {   // Check Image File Exist
                if (FileManager.default.fileExists(atPath: strDestinationPath) != true)
                {
                    URLSession.shared.downloadTask(with: urlImageURL!)
                    {
                        location, response, error in guard let location = location else
                        {
                            objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "Download image file error \(urlImageURL!.lastPathComponent)")
                            return
                        }
                        do
                        {
                            // Check Image File Exist
                            if (FileManager.default.fileExists(atPath: strDestinationPath) != true)
                            {
                                try FileManager.default.moveItem(at: location, to: URL(fileURLWithPath: strDestinationPath))
                                objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "Download image file : \(urlImageURL!.lastPathComponent)")
                            }
                        }
                        catch
                        {
                            objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "Download image file error \(urlImageURL!.lastPathComponent) : \(error)")
                        }
                   }.resume()
                }
            }
        }
    }
}
