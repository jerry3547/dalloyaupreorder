//
//  StructMaster.swift
//  DalloyauPreOrder
//
//  Created by jerry on 27/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

extension UIDevice {
    
    private struct InterfaceNames {
        static let wifi = ["en0"]
        static let wired = ["en2", "en3", "en4"]
        static let cellular = ["pdp_ip0","pdp_ip1","pdp_ip2","pdp_ip3"]
        static let supported = wifi + wired + cellular
    }
    
    func ipAddress() -> String? {
        var ipAddress: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>?
        
        if getifaddrs(&ifaddr) == 0 {
            var pointer = ifaddr
            
            while pointer != nil {
                defer { pointer = pointer?.pointee.ifa_next }
                
                guard
                    let interface = pointer?.pointee,
                    interface.ifa_addr.pointee.sa_family == UInt8(AF_INET) || interface.ifa_addr.pointee.sa_family == UInt8(AF_INET6),
                    let interfaceName = interface.ifa_name,
                    let interfaceNameFormatted = String(cString: interfaceName, encoding: .utf8),
                    InterfaceNames.supported.contains(interfaceNameFormatted)
                    else { continue }
                
                var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                
                getnameinfo(interface.ifa_addr,
                            socklen_t(interface.ifa_addr.pointee.sa_len),
                            &hostname,
                            socklen_t(hostname.count),
                            nil,
                            socklen_t(0),
                            NI_NUMERICHOST)
                
                guard
                    let formattedIpAddress = String(cString: hostname, encoding: .utf8),
                    !formattedIpAddress.isEmpty
                    else { continue }
                
                ipAddress = formattedIpAddress
                break
            }
            
            freeifaddrs(ifaddr)
        }
        
        return ipAddress
    }
}

class StructMaster {
    struct DeviceInformationStruct {
        var strDataSource : String = ""
        var uidDeviceUDID : String = ""
        var strIOSVersion : String = ""
        var strIPAddress : String = ""
        var strAppVersion : String = ""
        var strAdminPassword : String = ""
        var strStoreCode : String = ""
        var strStoreDesc : String = ""
        var bitEnabled : Bool = false
        var dtmLastLoginDate : Date = Date()
        var strCreatedUser : String = ""
        var strCreatedUserName : String = ""
        var dtmCreatedDate : Date = Date()
        var strModifiedUser : String = ""
        var strModifiedUserName : String = ""
        var dtmModifiedDate : Date = Date()
        
        init() {
            let objSQLite : SQLiteMaster = SQLiteMaster()
            self.uidDeviceUDID = UIDevice.current.identifierForVendor!.uuidString
            self.strIOSVersion = UIDevice.current.systemVersion
            self.strAppVersion = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] ?? "") as! String
            self.strIPAddress = UIDevice.current.ipAddress()!
            self.strStoreCode = objSQLite.ReadSettingConcurrenceControlValue(pstrFunctionGroup: "DeviceInfo", pstrFunctionObject: "strStoreCode")
        }
        
        func Update(){
            let _ : StructMaster.Table.tblSettingConcurrenceControlStructList.tblSettingConcurrenceControlStruct = StructMaster.Table.tblSettingConcurrenceControlStructList.tblSettingConcurrenceControlStruct(pintDisplayOrder : 1, pstrFunctionGroup : "DeviceInfo", pstrFunctionObject : "uidDeviceUDID", pstrFunctionValue : self.uidDeviceUDID)
            let _ : StructMaster.Table.tblSettingConcurrenceControlStructList.tblSettingConcurrenceControlStruct = StructMaster.Table.tblSettingConcurrenceControlStructList.tblSettingConcurrenceControlStruct(pintDisplayOrder : 1, pstrFunctionGroup : "DeviceInfo", pstrFunctionObject : "strIOSVersion", pstrFunctionValue : self.strIOSVersion)
            let _ : StructMaster.Table.tblSettingConcurrenceControlStructList.tblSettingConcurrenceControlStruct = StructMaster.Table.tblSettingConcurrenceControlStructList.tblSettingConcurrenceControlStruct(pintDisplayOrder : 1, pstrFunctionGroup : "DeviceInfo", pstrFunctionObject : "strAppVersion", pstrFunctionValue : self.strAppVersion)
            let _ : StructMaster.Table.tblSettingConcurrenceControlStructList.tblSettingConcurrenceControlStruct = StructMaster.Table.tblSettingConcurrenceControlStructList.tblSettingConcurrenceControlStruct(pintDisplayOrder : 1, pstrFunctionGroup : "DeviceInfo", pstrFunctionObject : "strIPAddress", pstrFunctionValue : self.strIPAddress)
            let _ : StructMaster.Table.tblSettingConcurrenceControlStructList.tblSettingConcurrenceControlStruct = StructMaster.Table.tblSettingConcurrenceControlStructList.tblSettingConcurrenceControlStruct(pintDisplayOrder : 1, pstrFunctionGroup : "DeviceInfo", pstrFunctionObject : "strStoreCode", pstrFunctionValue : self.strStoreCode)
        }
    }
    
    class Table {
        struct tblSettingConcurrenceControlStructList {
            var Detail : [tblSettingConcurrenceControlStruct] = []
            
            mutating func Update() {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                for tblSettingConcurrenceControl : tblSettingConcurrenceControlStruct in Detail
                {
                    objSQLite.UpdateSettingConcurrenceControl(ptblSettingConcurrenceControl : tblSettingConcurrenceControl)
                }
            }
            
            struct tblSettingConcurrenceControlStruct {
                var intDisplayOrder : Int = 0
                var strFunctionGroup : String = ""
                var strFunctionObject : String = ""
                var strFunctionValue : String = ""
                var dtmModifiedDate : Date = Date()
                
                init() {}
                
                init(pintDisplayOrder : Int, pstrFunctionGroup : String, pstrFunctionObject : String, pstrFunctionValue : String) {
                    self.intDisplayOrder = pintDisplayOrder
                    self.strFunctionGroup = pstrFunctionGroup
                    self.strFunctionObject = pstrFunctionObject
                    self.strFunctionValue = pstrFunctionValue
                    self.dtmModifiedDate = Date()
                    self.Update()
                }
                
                mutating func Update() {
                    let objSQLite : SQLiteMaster = SQLiteMaster()
                    objSQLite.UpdateSettingConcurrenceControl(ptblSettingConcurrenceControl : self)
                }
            }
        }
        
        struct tblPOSItemStructList {
            var Detail : [tblPOSItemStruct] = []
            
            mutating func Update() {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                for tblPOSItem : tblPOSItemStruct in Detail
                {
                    objSQLite.UpdatePOSItem(ptblPOSItem : tblPOSItem)
                }
            }
            
            struct tblPOSItemStruct {
                var strItemCode : String = ""
                var strItemDesc : String = ""
                var strItemRemark : String = ""
                var decOriginAmt : Double = 0.0
                var decSalesAmt : Double = 0.0
                var intDeliveryDay : Int64 = 0
                var dtmPublicDateFm : Date = Date()
                var dtmPublicDateTo : Date = Date()
                
                init(){}
                
                init(pstrItemCode : String, pstrItemDesc : String, pstrItemRemark : String, pdecOriginAmt : Double, pdecSalesAmt : Double, pintDeliveryDay : Int64, pdtmPublicDateFm : Date, pdtmPublicDateTo : Date = Date()){
                    self.strItemCode = pstrItemCode
                    self.strItemDesc = pstrItemDesc
                    self.strItemRemark = pstrItemRemark
                    self.decOriginAmt = pdecOriginAmt
                    self.decSalesAmt = pdecSalesAmt
                    self.intDeliveryDay  = pintDeliveryDay
                    self.dtmPublicDateFm = pdtmPublicDateFm
                    self.dtmPublicDateTo = pdtmPublicDateTo
                }
            
                mutating func Update() {
                    let objSQLite : SQLiteMaster = SQLiteMaster()
                    objSQLite.UpdatePOSItem(ptblPOSItem : self)
                }
                
                mutating func Read() -> tblPOSItemStruct {
                    let objSQLite : SQLiteMaster = SQLiteMaster()
                    self = objSQLite.ReadPOSItem(pstrItemCode : self.strItemCode)
                    
                    return self
                }
            }
        }
     
        struct tblPOSItemTypeConfigStructList {
            var Detail : [tblPOSItemTypeConfigStruct] = []
            
            mutating func Update() {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                for tblPOSItemTypeConfig : tblPOSItemTypeConfigStruct in Detail
                {
                    objSQLite.UpdatePOSItemTypeConfig(ptblPOSItemTypeConfig : tblPOSItemTypeConfig)
                }
            }
            
            struct tblPOSItemTypeConfigStruct {
                // tblPOSItemTypeConfig //
                var strItemType : String = ""
                var strTypeCode : String = ""
                var strTypeDesc : String = ""
                var strTypeRemark : String = ""
                var intDisplayOrder : Int64 = 0
                var intAddOnCount : Int64 = 0
                var dtmPublicDateFm : Date = Date()
                var dtmPublicDateTo : Date = Date()
                
                mutating func Update() {
                    let objSQLite : SQLiteMaster = SQLiteMaster()
                    objSQLite.UpdatePOSItemTypeConfig(ptblPOSItemTypeConfig : self)
                }
            }
        }
    
        struct tblPOSItemCakeConfigAttributeStructList {
            var Detail : [tblPOSItemCakeConfigAttributeStruct] = []
            
            mutating func Update() {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                for tblPOSItemCakeConfigAttribute : tblPOSItemCakeConfigAttributeStruct in Detail
                {
                    objSQLite.UpdatePOSItemCakeConfigAttribute(ptblPOSItemCakeConfigAttribute : tblPOSItemCakeConfigAttribute)
                }
            }
            
            struct tblPOSItemCakeConfigAttributeStruct {
                var strCakeCode : String = ""
                var strItemCode : String = ""
                var strCakeAttributeDescPrint : String = ""
                var strCakeAttributeDescFlavor : String = ""
                var strCakeAttributeDescSize : String = ""
                var intDisplayOrder : Int64 = 0
                
                mutating func Update()
                {
                    let objSQLite : SQLiteMaster = SQLiteMaster()
                    objSQLite.UpdatePOSItemCakeConfigAttribute(ptblPOSItemCakeConfigAttribute : self)
                }
            }
        }
        
        struct tblPOSItemImageStructList {
            var Detail : [tblPOSItemImageStruct] = []
            
            mutating func Update() {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                for tblPOSItemImage : tblPOSItemImageStruct in Detail
                {
                    objSQLite.UpdatePOSItemImage(ptblPOSItemImage : tblPOSItemImage)
                }
            }
            
            mutating func Read() {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                self = objSQLite.ReadPOSItemImage()
            }
            
            mutating func Read(pstrItemCode : String) {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                self = objSQLite.ReadPOSItemImage(pstrItemCode : pstrItemCode)
            }
            
            mutating func Read(pstrTypeCode : String) {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                self = objSQLite.ReadPOSItemImage(pstrTypeCode : pstrTypeCode)
            }
            
            struct tblPOSItemImageStruct {
                var strItemCode : String = ""
                var strImageURL : String = ""
                
                mutating func Update()
                {
                    let objSQLite : SQLiteMaster = SQLiteMaster()
                    objSQLite.UpdatePOSItemImage(ptblPOSItemImage : self)
                }
                
                func GetUIImage() -> UIImage{
                    let strImageFilePath : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/ItemImages/"
                    let strImageFileName : String = URL(string : self.strImageURL)!.lastPathComponent
                    let strImagePath : String = strImageFilePath + strImageFileName
                    
                    if (FileManager.default.fileExists(atPath : strImagePath) == true)
                    {
                        let urlImagePath = URL(fileURLWithPath: strImagePath)
                        let objImageData : Data = try! Data(contentsOf : urlImagePath)
                        
                        return UIImage(data : objImageData)!
                    }
                    else
                    {
                        return UIImage(named: "sysError.png")!
                    }
                }
            }
        }
        
        struct tblPOSStoreStructList {
            var Detail : [tblPOSStoreStruct] = []
            
            mutating func Update() {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                for tblPOSStore : tblPOSStoreStruct in Detail
                {
                    objSQLite.UpdatePOSStore(ptblPOSStore : tblPOSStore)
                }
            }
            
            mutating func Read()
            {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                self = objSQLite.ReadPOSStore()
            }
            
            struct tblPOSStoreStruct {
                var strStoreCode : String = ""
                var strStoreDesc : String = ""
                var strStoreAddress : String = ""
                var strStorePhone : String = ""
                var strStoreBusinessHour : String = ""
                var strStoreImageURL : String = ""
                
                mutating func Update() {
                    let objSQLite : SQLiteMaster = SQLiteMaster()
                    objSQLite.UpdatePOSStore(ptblPOSStore : self)
                }
                
                func GetUIImage() -> UIImage{
                    let strImageFilePath : String = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/StoreImages/"
                    let strImageFileName : String = URL(string : self.strStoreImageURL)!.lastPathComponent
                    let strImagePath : String = strImageFilePath + strImageFileName
                    
                    if (FileManager.default.fileExists(atPath : strImagePath) == true)
                    {
                        let urlImagePath = URL(fileURLWithPath: strImagePath)
                        let objImageData : Data = try! Data(contentsOf : urlImagePath)
                        
                        return UIImage(data : objImageData)!
                    }
                    else
                    {
                        return UIImage(named: "sysError.png")!
                    }
                }
            }            
        }
        
        struct tblPOSLollipopMessageStructList {
            var Detail : [tblPOSLollipopMessageStruct] = []
            
            mutating func Update() {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                for tblPOSLollipopMessage : tblPOSLollipopMessageStruct in Detail
                {
                    objSQLite.UpdatePOSLollipopMessage(pblPOSLollipopMessage : tblPOSLollipopMessage)
                }
            }
            
            struct tblPOSLollipopMessageStruct {
                var strMessageID : String = ""
                var strMessageDesc : String = ""
                var strParentID : String = ""
                var intDisplayOrder : Int64 = 0
                
                mutating func Update() {
                    let objSQLite : SQLiteMaster = SQLiteMaster()
                    objSQLite.UpdatePOSLollipopMessage(pblPOSLollipopMessage : self)
                }
            }
        }
    }
    
    class PreOrder {
        struct PreOrderItemStructList {
            var Detail : [PreOrderItemStruct] = []
            
            mutating func Read(pstrItemType : String) {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                self = objSQLite.ReadPreOrderItem(pstrItemType: pstrItemType)
            }
            
            struct PreOrderItemStruct {
                var strItemType : String = ""
                var strTypeCode : String = ""
                var strTypeDesc : String = ""
                var strTypeRemark : String = ""
                var intAddOnCount : Int64 = 0
                var intDisplayOrder : Int64 = 0
                var decSalesAmtFm : Double = 0.0
                var decSalesAmtTo : Double = 0.0
                var intDeliveryDayFm : Int64 = 0
                var intDeliveryDayTo : Int64 = 0
                
                mutating func Read(pstrItemType : String, pstrTypeCode : String) {
                    let objSQLite : SQLiteMaster = SQLiteMaster()
                    self = objSQLite.ReadPreOrderItem(pstrItemType: pstrItemType, pstrTypeCode: pstrTypeCode)
                }
            }
        }
        
        struct PreOrderItemCakeStructList {
            var Detail : [PreOrderItemCakeStruct] = []
            
            mutating func Read(pstrItemType : String) {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                self = objSQLite.ReadPreOrderItemCake()
            }
            
            struct PreOrderItemCakeStruct {
                var strItemType : String = ""
                var strTypeCode : String = ""
                var strTypeDesc : String = ""
                var strTypeRemark : String = ""
                var intAddOnCount : Int64 = 0
                var intDisplayOrder : Int64 = 0
                var intDetailCount : Int64 = 0
                var decSalesAmtFm : Double = 0.0
                var decSalesAmtTo : Double = 0.0
                var intDeliveryDayFm : Int64 = 0
                var intDeliveryDayTo : Int64 = 0
                var CakeItemList : [PreOrderItemCakeDetailStruct] = []
                
                struct PreOrderItemCakeDetailStruct {
                    var strItemCode : String = ""
                    var strItemDesc : String = ""
                    var strItemRemark : String = ""
                    var strCakeAttributeDescPrint : String = ""
                    var strCakeAttributeDescFlavor : String = ""
                    var strCakeAttributeDescSize : String = ""
                    var intDisplayOrder : Int64 = 0
                    var decOriginAmt : Double = 0.0
                    var decSalesAmt : Double = 0.0
                }
                
                mutating func Read(pstrTypeCode : String) {
                    let objSQLite : SQLiteMaster = SQLiteMaster()
                    self = objSQLite.ReadPreOrderItemCake(pstrTypeCode: pstrTypeCode)
                }
                
                mutating func GetCakeAttribute(pstrAttributeType : String) -> [String] {
                    let objSQLite : SQLiteMaster = SQLiteMaster()
                    return objSQLite.ReadCakeAttribute(pstrCakeCode : strTypeCode, pstrAttributeType: pstrAttributeType)
                }
                
                mutating func GetCakeItemList(pstrAttributePrint : String, pstrAttributeFlavor : String, pstrAttributeSize : String) {
                    let objSQLite : SQLiteMaster = SQLiteMaster()
                    self = objSQLite.ReadPreOrderItemCakeDetail(pstrCakeCode: self.strTypeCode, pstrAttributePrint : pstrAttributePrint, pstrAttributeFlavor : pstrAttributeFlavor, pstrAttributeSize : pstrAttributeSize)
                }
            }
        }
        
        struct PreOrderItemCakeMessageStructList {
            var Detail : [PreOrderItemCakeMessageStruct] = []
           
            mutating func Read(pstrParentID : String) {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                self.Detail = objSQLite.ReadPreOrderLollipopMessage(pstrParentID : pstrParentID)
            }
            
            struct PreOrderItemCakeMessageStruct {
                var strMessageID : String = ""
                var strMessageDesc : String = ""
                var intDisplayOrder : Int64 = 0
                var intChildCount : Int64 = 0
                var ChildMessage : [PreOrderItemCakeMessageStruct] = []
            }
        }
        
        struct PreOrderSalesHeaderStruct {
            // Sales Header //
            var uidDeviceUDID : String = ""
            var strStoreCode : String = ""
            var strStoreDesc : String = ""
            var strPreOrderDocNo : String = ""
            var dtmPreOrderDate : Date = Date()
            var dtmPreOrderTime : Date = Date()
            var decSalesQty : Double = 0.0
            var decSalesAmt : Double = 0.0
            var strOrderStatus : String = ""
            // Customer Information //
            var strCustTitle : String = ""
            var strCustNameFirst : String = ""
            var strCustNameLst : String = ""
            var strCustPhoneH : String = ""
            var strCustPhoneM : String = ""
            var strCustEMail : String = ""
            // Bindo Information //
            var strBindoReference : String = ""
            var dtmBindoDate : Date = Date()
            var dtmBindoTime : Date = Date()
            var strBindoStatus : String = ""
            
            var SalesDetailList : [PreOrderSalesDetailStruct] = []
            
            init () {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                let objPreOrderDocNoFormatter : DateFormatter = DateFormatter()
                objPreOrderDocNoFormatter.dateFormat = "yyyyMMddHHmmss"
                self.uidDeviceUDID = objSQLite.ReadSettingConcurrenceControlValue(pstrFunctionGroup: "DeviceInfo", pstrFunctionObject: "uidDeviceUDID")
                self.strStoreCode = objSQLite.ReadSettingConcurrenceControlValue(pstrFunctionGroup: "DeviceInfo", pstrFunctionObject: "strStoreCode")
                self.strStoreDesc = objSQLite.ReadPOSStore(pstrStoreCode : self.strStoreCode).strStoreDesc
                self.strPreOrderDocNo = objPreOrderDocNoFormatter.string(from: Date())
            }
            
            mutating func Update() {
                print("PreOrderSalesHeaderStruct.Update")
            }
            
            mutating func AddNewDetail(pstrItemType : String, pstrItemCode : String) -> Int {
                switch (pstrItemType)
                {
                case "Pastrie", "Canape", "Festive":
                    var intRowIndex : Int = 0
                    for PreOrderSalesDetail : PreOrderSalesDetailStruct in SalesDetailList
                    {
                        if (PreOrderSalesDetail.strItemType == pstrItemType && PreOrderSalesDetail.strItemCode == pstrItemCode)
                        {
                            print("PreOrderSalesDetail.AddNewDetail.Edit.Detail (Index : \(intRowIndex), Type : \(pstrItemType), Item : \(pstrItemCode))")
                            return intRowIndex
                        }
                        intRowIndex = intRowIndex + 1
                    }
                    var NewPreOrderSalesDetail : PreOrderSalesDetailStruct = PreOrderSalesDetailStruct()
                    var PreOrderItem : PreOrderItemStructList.PreOrderItemStruct = PreOrderItemStructList.PreOrderItemStruct()
                    PreOrderItem.Read(pstrItemType: pstrItemType, pstrTypeCode: pstrItemCode)
                    NewPreOrderSalesDetail.intLineNo = Int64(intRowIndex)
                    NewPreOrderSalesDetail.strItemType = pstrItemType
                    NewPreOrderSalesDetail.strItemCode = pstrItemCode
                    NewPreOrderSalesDetail.strItemDesc = PreOrderItem.strTypeDesc
                    NewPreOrderSalesDetail.strItemRemark = PreOrderItem.strTypeRemark
                    NewPreOrderSalesDetail.decUnitPrice = PreOrderItem.decSalesAmtFm
                    NewPreOrderSalesDetail.decSalesQty = 0.0
                    NewPreOrderSalesDetail.decSalesAmt = 0.0
                    SalesDetailList.append(NewPreOrderSalesDetail)
                    print("PreOrderSalesDetail.AddNewDetail.AddNew.Detail (Index : \(intRowIndex), Type : \(pstrItemType), Item : \(pstrItemCode))")
                    return intRowIndex
                case "Cake":
                    var intRowIndex : Int = 0
                    for _ : PreOrderSalesDetailStruct in SalesDetailList
                    {
                        intRowIndex = intRowIndex + 1
                    }
                    var NewPreOrderSalesDetail : PreOrderSalesDetailStruct = PreOrderSalesDetailStruct()
                    var PreOrderItem : PreOrderItemStructList.PreOrderItemStruct = PreOrderItemStructList.PreOrderItemStruct()
                    PreOrderItem.Read(pstrItemType: pstrItemType, pstrTypeCode: pstrItemCode)
                    NewPreOrderSalesDetail.intLineNo = Int64(intRowIndex)
                    NewPreOrderSalesDetail.strItemType = pstrItemType
                    NewPreOrderSalesDetail.strItemCode = ""
                    NewPreOrderSalesDetail.strItemDesc = ""
                    NewPreOrderSalesDetail.strItemRemark = ""
                    NewPreOrderSalesDetail.decUnitPrice = 0.0
                    NewPreOrderSalesDetail.decSalesQty = 0.0
                    NewPreOrderSalesDetail.decSalesAmt = 0.0
                    SalesDetailList.append(NewPreOrderSalesDetail)
                    print("PreOrderSalesDetail.AddNewDetail.AddNew.Cake (Index : \(intRowIndex), Type : \(pstrItemType), Item : \(pstrItemCode))")
                    return intRowIndex
                case "Delivery":
                    return 0
                default:
                    return 0
                }
            }
            
            func GetSalesQty() -> Double {
                var decSalesQty : Double = 0.0
                for SalesDetail : PreOrderSalesDetailStruct in SalesDetailList {
                    decSalesQty = decSalesQty + SalesDetail.decSalesQty
                }
                return decSalesQty
            }
            
            func GetSalesQty(pstrItemType : String, pstrItemCode : String) -> Double {
                let objSQLite : SQLiteMaster = SQLiteMaster()
                var decSalesQty : Double = 0.0
                for SalesDetail : PreOrderSalesDetailStruct in SalesDetailList {
                    switch(pstrItemType)
                    {
                    case "Cake":
                        if (SalesDetail.strItemType == pstrItemType && objSQLite.ReadCakeCode(pstrItemCode: SalesDetail.strItemCode) == pstrItemCode)
                        {
                            decSalesQty = decSalesQty + SalesDetail.decSalesQty
                        }
                        break
                    default:
                        if (SalesDetail.strItemType == pstrItemType && SalesDetail.strItemCode == pstrItemCode)
                        {
                            decSalesQty = decSalesQty + SalesDetail.decSalesQty
                        }
                        break
                    }
                }
                return decSalesQty
            }
        }
        
        struct PreOrderSalesDetailStruct {
            var intLineNo : Int64 = 0
            var strItemType : String = ""
            var strItemCode : String = ""
            var strItemDesc : String = ""
            var strItemRemark : String = ""
            var decUnitPrice : Double = 0.0
            var decSalesQty : Double = 0.0
            var decSalesAmt : Double = 0.0
            
            var AddOnDetailList : [PreOrderSalesDetailAddOnStruct] = []
        }
        
        struct PreOrderSalesDetailAddOnStruct {
            var intLineNo : Int64 = 0
            var intAddOnLineNo : Int64 = 0
            var strItemType : String = ""
            var strItemCode : String = ""
            var strItemDesc : String = ""
            var strItemRemark : String = ""
            var strCustRemark : String = ""
            var decUnitPrice : Double = 0.0
            var decSalesQty : Double = 0.0
            var decSalesAmt : Double = 0.0
        }
    }
}
