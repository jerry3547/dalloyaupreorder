//
//  WebAPIMaster.swift
//  DalloyauPreOrder
//
//  Created by jerry on 27/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class WebAPIDataMaster {
    let strWebAPI : String = "http://rpt.apps-lib.com:8195/DalloyauPreOrder/Mobile/frmWebAPI.aspx"
    var DeviceInformation : StructMaster.DeviceInformationStruct = StructMaster.DeviceInformationStruct()
    private var objSystemLog : SystemIOMaster.SystemLog = SystemIOMaster.SystemLog()
    private var objSQLite : SQLiteMaster = SQLiteMaster()
   
    func APIUpdateDeviceInformation(strStatusMessage : @escaping (String) -> (Void)) {
        let strWebAPIPath : String = "?pstrAction=DeviceInformation&puidDeviceUDID=\(DeviceInformation.uidDeviceUDID)&pstrAppVersion=\(DeviceInformation.strAppVersion)&pstrIOSVersion=\(DeviceInformation.strIOSVersion)"
        let urlWebAPIPath : URL = URL(string : "\(strWebAPI)\(strWebAPIPath)")!
        var pstrStatusMessage : String = "Calling WebAPI \(strWebAPI)"
        strStatusMessage("\(pstrStatusMessage)")
        self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "Calling WebAPI \(strWebAPI)\(strWebAPIPath)")
        
        URLSession.shared.dataTask(with : urlWebAPIPath)
        {
            (data, response, error) in
            self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : strWebAPIPath)
            
            if error != nil{
                self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "Error URLSession : \(error!)")
                strStatusMessage("")
            }
            else
            {
                do
                {
                    let jsonWithArrayRoot = try JSONSerialization.jsonObject(with: data!)
                    
                    for object in (jsonWithArrayRoot as! [Any])
                    {
                        if let dictionary = object as? [String: Any]
                        {
                            // tblSettingConcurrenceControl //
                            var DeviceInformation : StructMaster.DeviceInformationStruct = StructMaster.DeviceInformationStruct()
                            DeviceInformation.uidDeviceUDID =  dictionary["uidDeviceUDID"] as? String ?? ""
                            DeviceInformation.strIOSVersion = dictionary["strIOSVersion"] as? String ?? ""
                            DeviceInformation.strAppVersion = dictionary["strAppVersion"] as? String ?? ""
                            DeviceInformation.strAdminPassword = dictionary["strAdminPassword"] as? String ?? ""
                            DeviceInformation.strStoreCode = dictionary["strStoreCode"] as? String ?? ""
                            DeviceInformation.dtmLastLoginDate = self.GetJSONDate(objBinding: dictionary["dtmLastLoginDate"] as? String ?? "")
                            DeviceInformation.Update()
                            
                            pstrStatusMessage = "1/11 Update loacl database completed (tblSettingConcurrenceControl)"
                            strStatusMessage("\(pstrStatusMessage)")
                            self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "\(pstrStatusMessage)")
                            
                            // tblPOSItem //
                            var intRowAffected : Int = 0
                            intRowAffected = self.ReadPOSItem(objJSONArray: dictionary["tblPOSItemList"] as! [Any])
                            pstrStatusMessage = "2/11 Update loacl database completed (tblPOSItemMaster [\(intRowAffected)])"
                            strStatusMessage("\(pstrStatusMessage)")
                            self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "\(pstrStatusMessage)")
                            
                            // tblPOSItemCakeConfig //
                            intRowAffected = self.ReadPOSItemTypeConfig(objJSONArray: dictionary["tblPOSItemTypeConfigList"] as! [Any])
                            pstrStatusMessage = "3/11 Update loacl database completed (tblPOSItemTypeConfig [\(intRowAffected)])"
                            strStatusMessage("\(pstrStatusMessage)")
                            self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "\(pstrStatusMessage)")
                            
                            // tblPOSItemCakeConfigAttribute //
                            intRowAffected = self.ReadPOSItemCakeConfigAttribute(objJSONArray: dictionary["tblPOSItemCakeConfigAttributeList"] as! [Any])
                            pstrStatusMessage = "4/11 Update loacl database completed (tblPOSItemCakeConfigAttribute [\(intRowAffected)])"
                            strStatusMessage("\(pstrStatusMessage)")
                            self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "\(pstrStatusMessage)")
                            
                            // tblPOSItemImage //
                            intRowAffected = self.ReadPOSItemImage(objJSONArray: dictionary["tblPOSItemImageList"] as! [Any])
                            pstrStatusMessage = "5/11 Update loacl database completed (tblPOSItemImage [\(intRowAffected)])"
                            strStatusMessage("\(pstrStatusMessage)")
                            self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "\(pstrStatusMessage)")
                            
                            // tblPOSStore //
                            intRowAffected = self.ReadPOSStore(objJSONArray: dictionary["tblPOSStoreList"] as! [Any])
                            pstrStatusMessage = "6/11 Update loacl database completed (tblPOSStore [\(intRowAffected)])"
                            strStatusMessage("\(pstrStatusMessage)")
                            self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "\(pstrStatusMessage)")
                            
                            // tblPOSLollipopMessage //
                            intRowAffected = self.ReadPOSLollipopMessage(objJSONArray: dictionary["tblPOSLollipopMessageList"] as! [Any])
                            pstrStatusMessage = "7/11 Update loacl database completed (tblPOSLollipopMessage [\(intRowAffected)])"
                            strStatusMessage("\(pstrStatusMessage)")
                            self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "\(pstrStatusMessage)")
                            
                            // tblPreOrderSalesHeader //
                            //intRowAffected = self.ReadPOSStore(objJSONArray: dictionary["tblPreOrderSalesHeader"] as! [Any])
                            intRowAffected = 0
                            pstrStatusMessage = "8/11 Update loacl database completed (tblPreOrderSalesHeader [\(intRowAffected)])"
                            strStatusMessage("\(pstrStatusMessage)")
                            self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "\(pstrStatusMessage)")
                            
                            // tblPreOrderSalesDetail //
                            //intRowAffected = self.ReadPOSStore(objJSONArray: dictionary["tblPreOrderSalesDetail"] as! [Any])
                            intRowAffected = 0
                            pstrStatusMessage = "9/11 Update loacl database completed (tblPreOrderSalesDetail [\(intRowAffected)])"
                            strStatusMessage("\(pstrStatusMessage)")
                            self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "\(pstrStatusMessage)")
                            
                            // tblPreOrderDeliveryHeader //
                            //intRowAffected = self.ReadPOSStore(objJSONArray: dictionary["tblPreOrderDeliveryHeader"] as! [Any])
                            intRowAffected = 0
                            pstrStatusMessage = "10/11 Update loacl database completed (tblPreOrderDeliveryHeader [\(intRowAffected)])"
                            strStatusMessage("\(pstrStatusMessage)")
                            self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "\(pstrStatusMessage)")
                            
                            // tblPreOrderDeliveryDetail //
                            //intRowAffected = self.ReadPOSStore(objJSONArray: dictionary["tblPreOrderDeliveryDetail"] as! [Any])
                            intRowAffected = 0
                            pstrStatusMessage = "11/11 Update loacl database completed (tblPreOrderDeliveryDetail [\(intRowAffected)])"
                            strStatusMessage("\(pstrStatusMessage)")
                            self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "\(pstrStatusMessage)")
                            
                            strStatusMessage("")
                        }
                    }
                }
                catch let error as NSError
                {
                    self.objSystemLog.WriteLogFile(pstrClassName : #file, pstrFunctionName : #function, pstrLogMessage : "Error Parsing Json \(error)")
                    strStatusMessage("")
                }
            }
        }.resume()
    }
    
    private func ReadDeviceInformation(objJSONArray : [Any]) -> Int {
        for tblJSONRow in (objJSONArray)
        {
            if let tblDetailRow = tblJSONRow as? [String: Any]
            {
                var DeviceInformationStruct : StructMaster.DeviceInformationStruct = StructMaster.DeviceInformationStruct()
                DeviceInformationStruct.uidDeviceUDID = tblDetailRow["strItemCode"] as? String ?? ""
                DeviceInformationStruct.strIOSVersion = tblDetailRow["strIOSVersion"] as? String ?? ""
                DeviceInformationStruct.strAppVersion = tblDetailRow["strAppVersion"] as? String ?? ""
                DeviceInformationStruct.strAdminPassword = tblDetailRow["strAdminPassword"] as? String ?? ""
                DeviceInformationStruct.strStoreCode = tblDetailRow["strStoreCode"] as? String ?? ""
                DeviceInformationStruct.Update()
            }
        }
        
        return 0
    }
    
    private func ReadPOSItem(objJSONArray : [Any]) -> Int {
        var tblPOSItemList : StructMaster.Table.tblPOSItemStructList = StructMaster.Table.tblPOSItemStructList()
        for tblJSONRow in (objJSONArray)
        {
            if let tblDetailRow = tblJSONRow as? [String: Any]
            {
                var tblPOSItem : StructMaster.Table.tblPOSItemStructList.tblPOSItemStruct = StructMaster.Table.tblPOSItemStructList.tblPOSItemStruct()
                tblPOSItem.strItemCode = tblDetailRow["strItemCode"] as? String ?? ""
                tblPOSItem.strItemDesc = tblDetailRow["strItemDesc"] as? String ?? ""
                tblPOSItem.strItemRemark = tblDetailRow["strItemRemark"] as? String ?? ""
                tblPOSItem.decOriginAmt = tblDetailRow["decOriginAmt"] as? Double ?? 0.0
                tblPOSItem.decSalesAmt = tblDetailRow["decSalesAmt"] as? Double ?? 0.0
                tblPOSItem.intDeliveryDay = tblDetailRow["intDeliveryDay"] as? Int64 ?? 0
                tblPOSItem.dtmPublicDateFm = self.GetJSONDate(objBinding: tblDetailRow["dtmPublicDateFm"] as? String ?? "")
                tblPOSItem.dtmPublicDateTo = self.GetJSONDate(objBinding: tblDetailRow["dtmPublicDateTo"] as? String ?? "")
                tblPOSItemList.Detail.append(tblPOSItem)
            }
        }
        
        tblPOSItemList.Update()
        return tblPOSItemList.Detail.count
    }
    
    private func ReadPOSItemTypeConfig(objJSONArray : [Any]) -> Int {
        var tblPOSItemTypeConfigList : StructMaster.Table.tblPOSItemTypeConfigStructList = StructMaster.Table.tblPOSItemTypeConfigStructList()
        for tblJSONRow in (objJSONArray)
        {
            if let tblDetailRow = tblJSONRow as? [String: Any]
            {
                var tblPOSItemTypeConfig : StructMaster.Table.tblPOSItemTypeConfigStructList.tblPOSItemTypeConfigStruct = StructMaster.Table.tblPOSItemTypeConfigStructList.tblPOSItemTypeConfigStruct()
                tblPOSItemTypeConfig.strItemType = tblDetailRow["strItemType"] as? String ?? ""
                tblPOSItemTypeConfig.strTypeCode = tblDetailRow["strTypeCode"] as? String ?? ""
                tblPOSItemTypeConfig.strTypeDesc = tblDetailRow["strTypeDesc"] as? String ?? ""
                tblPOSItemTypeConfig.strTypeRemark = tblDetailRow["strTypeRemark"] as? String ?? ""
                tblPOSItemTypeConfig.intDisplayOrder = tblDetailRow["intDisplayOrder"] as? Int64 ?? 0
                tblPOSItemTypeConfig.intAddOnCount = tblDetailRow["intAddOnCount"] as? Int64 ?? 0
                tblPOSItemTypeConfig.dtmPublicDateFm = self.GetJSONDate(objBinding: tblDetailRow["dtmPublicDateFm"] as? String ?? "")
                tblPOSItemTypeConfig.dtmPublicDateTo = self.GetJSONDate(objBinding: tblDetailRow["dtmPublicDateTo"] as? String ?? "")
                tblPOSItemTypeConfigList.Detail.append(tblPOSItemTypeConfig)
            }
        }
        
        tblPOSItemTypeConfigList.Update()
        return tblPOSItemTypeConfigList.Detail.count
    }
    
    private func ReadPOSItemCakeConfigAttribute(objJSONArray : [Any]) -> Int {
        var tblPOSItemCakeConfigAttributeList : StructMaster.Table.tblPOSItemCakeConfigAttributeStructList = StructMaster.Table.tblPOSItemCakeConfigAttributeStructList()
        for tblJSONRow in (objJSONArray)
        {
            if let tblDetailRow = tblJSONRow as? [String: Any]
            {
                var tblPOSItemCakeConfigAttribute : StructMaster.Table.tblPOSItemCakeConfigAttributeStructList.tblPOSItemCakeConfigAttributeStruct = StructMaster.Table.tblPOSItemCakeConfigAttributeStructList.tblPOSItemCakeConfigAttributeStruct()
                tblPOSItemCakeConfigAttribute.strCakeCode = tblDetailRow["strCakeCode"] as? String ?? ""
                tblPOSItemCakeConfigAttribute.strItemCode = tblDetailRow["strItemCode"] as? String ?? ""
                tblPOSItemCakeConfigAttribute.strCakeAttributeDescPrint = tblDetailRow["strCakeAttributeDescPrint"] as? String ?? ""
                tblPOSItemCakeConfigAttribute.strCakeAttributeDescFlavor = tblDetailRow["strCakeAttributeDescFlavor"] as? String ?? ""
                tblPOSItemCakeConfigAttribute.strCakeAttributeDescSize = tblDetailRow["strCakeAttributeDescSize"] as? String ?? ""
                tblPOSItemCakeConfigAttribute.intDisplayOrder = tblDetailRow["intDisplayOrder"] as? Int64 ?? 0
                tblPOSItemCakeConfigAttributeList.Detail.append(tblPOSItemCakeConfigAttribute)
            }
        }
        
        tblPOSItemCakeConfigAttributeList.Update()
        return tblPOSItemCakeConfigAttributeList.Detail.count
    }
    
    private func ReadPOSItemImage(objJSONArray : [Any]) -> Int {
        var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
        for tblJSONRow in (objJSONArray)
        {
            if let tblDetailRow = tblJSONRow as? [String: Any]
            {
                var tblPOSItemImage : StructMaster.Table.tblPOSItemImageStructList.tblPOSItemImageStruct = StructMaster.Table.tblPOSItemImageStructList.tblPOSItemImageStruct()
                tblPOSItemImage.strItemCode = tblDetailRow["strItemCode"] as? String ?? ""
                tblPOSItemImage.strImageURL = tblDetailRow["strImageURL"] as? String ?? ""
                tblPOSItemImageList.Detail.append(tblPOSItemImage)
            }
        }
        
        tblPOSItemImageList.Update()
        return tblPOSItemImageList.Detail.count
    }
    
    private func ReadPOSStore(objJSONArray : [Any]) -> Int {
        var tblPOSStoreList : StructMaster.Table.tblPOSStoreStructList = StructMaster.Table.tblPOSStoreStructList()
        for tblJSONRow in (objJSONArray)
        {
            if let tblDetailRow = tblJSONRow as? [String: Any]
            {
                var tblPOSStore : StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct = StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct()
                tblPOSStore.strStoreCode = tblDetailRow["strStoreCode"] as? String ?? ""
                tblPOSStore.strStoreDesc = tblDetailRow["strStoreDesc"] as? String ?? ""
                tblPOSStore.strStoreAddress = tblDetailRow["strStoreAddress"] as? String ?? ""
                tblPOSStore.strStorePhone = tblDetailRow["strStorePhone"] as? String ?? ""
                tblPOSStore.strStoreBusinessHour = tblDetailRow["strStoreBusinessHour"] as? String ?? ""
                tblPOSStore.strStoreImageURL = tblDetailRow["strStoreImageURL"] as? String ?? ""
                tblPOSStoreList.Detail.append(tblPOSStore)
            }
        }
        
        tblPOSStoreList.Update()
        return tblPOSStoreList.Detail.count
    }
    
    private func ReadPOSLollipopMessage(objJSONArray : [Any]) -> Int {
        var tblPOSLollipopMessageList : StructMaster.Table.tblPOSLollipopMessageStructList = StructMaster.Table.tblPOSLollipopMessageStructList()
        for tblJSONRow in (objJSONArray)
        {
            if let tblDetailRow = tblJSONRow as? [String: Any]
            {
                var tblPOSLollipopMessage : StructMaster.Table.tblPOSLollipopMessageStructList.tblPOSLollipopMessageStruct = StructMaster.Table.tblPOSLollipopMessageStructList.tblPOSLollipopMessageStruct()
                tblPOSLollipopMessage.strMessageID = tblDetailRow["strMessageID"] as? String ?? ""
                tblPOSLollipopMessage.strMessageDesc = tblDetailRow["strMessageDesc"] as? String ?? ""
                tblPOSLollipopMessage.strParentID = tblDetailRow["strParentID"] as? String ?? ""
                tblPOSLollipopMessage.intDisplayOrder = tblDetailRow["intDisplayOrder"] as? Int64 ?? 0
                tblPOSLollipopMessageList.Detail.append(tblPOSLollipopMessage)
            }
        }
        
        tblPOSLollipopMessageList.Update()
        return tblPOSLollipopMessageList.Detail.count
    }
    
    private func GetJSONDate(objBinding : String) -> Date {
        if (objBinding == "")
        {
            return Date()
        }
        
        let objDateFormatter = DateFormatter()
        objDateFormatter.dateFormat = "yyyy/MM/dd"
        
        if let dtmReturn : Date = objDateFormatter.date(from: objBinding)
        {
            return dtmReturn
        }
        else
        {
            return Date()
        }
    }
    
    private func GetJSONTime(objBinding : String) -> Date {
        if (objBinding == "")
        {
            return Date()
        }
        
        let objTimeFormatter = DateFormatter()
        objTimeFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        
        if let dtmReturn : Date = objTimeFormatter.date(from: objBinding)
        {
            return dtmReturn
        }
        else
        {
            return Date()
        }
    }
}

class WebAPIBindoInterface {
    let strWebAPI : String = "http://128.1.1.8/DalloyauPreOrder/Mobile/frmWebAPI.aspx"
    
    init() {
    }
}
