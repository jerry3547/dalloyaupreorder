//
//  frmPreOrderMainMenu.swift
//  DalloyauPreOrder
//
//  Created by jerry on 26/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderMainMenu : UIViewController
{
    var PreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct = StructMaster.PreOrder.PreOrderSalesHeaderStruct()
    
    // Navigation //
    @IBOutlet var ViewNavigationBarHeader : UIView!
    @IBOutlet var ViewNavigationBarFooter : UIView!
    var NavigationBarHeader : frmPreOrderNavigationHeader = frmPreOrderNavigationHeader()
    var NavigationBarFooter : frmPreOrderNavigationFooter = frmPreOrderNavigationFooter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataBind(pPreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct())
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is frmPreOrderNavigationHeader
        {
            NavigationBarHeader = segue.destination as! frmPreOrderNavigationHeader
        }
        
        if segue.destination is frmPreOrderNavigationFooter
        {
            NavigationBarFooter = segue.destination as! frmPreOrderNavigationFooter
        }
    }
    
    func DataBind(pPreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct) {
        PreOrderSalesHeader = pPreOrderSalesHeader
        NavigationBarHeader.DataBind(pPreOrderSalesHeader: PreOrderSalesHeader)
        NavigationBarFooter.DataBind(pPreOrderSalesHeader: PreOrderSalesHeader)
    }
    
    func ContainerViewSystemMenuShow(pbitIsHidden : Bool) {
        if (pbitIsHidden == true)
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 50)
        }
        else
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        }
    }
    
    @IBAction func cmdMainMenuCake_Click(sender : UIButton) {
        let objViewController : frmPreOrderItemList = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderItemList") as! frmPreOrderItemList
        self.present(objViewController, animated: true, completion: nil)
        objViewController.DataBind(pPreOrderSalesHeader : PreOrderSalesHeader, pstrItemType : "Cake")
    }
    
    @IBAction func cmdMainMenuPastries_Click(sender : UIButton) {
        let objViewController : frmPreOrderItemList = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderItemList") as! frmPreOrderItemList
        self.present(objViewController, animated: true, completion: nil)
        objViewController.DataBind(pPreOrderSalesHeader : PreOrderSalesHeader, pstrItemType : "Pastrie")
    }
    
    @IBAction func cmdMainMenuCanape_Click(sender : UIButton) {
        let objViewController : frmPreOrderItemList = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderItemList") as! frmPreOrderItemList
        self.present(objViewController, animated: true, completion: nil)
        objViewController.DataBind(pPreOrderSalesHeader : PreOrderSalesHeader, pstrItemType : "Canape")
    }
    
    @IBAction func cmdMainMenuFestive_Click(sender : UIButton) {
        let objViewController : frmPreOrderItemList = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderItemList") as! frmPreOrderItemList
        self.present(objViewController, animated: true, completion: nil)
        objViewController.DataBind(pPreOrderSalesHeader : PreOrderSalesHeader, pstrItemType : "Festive")
    }
    
    @IBAction func cmdMainMenuMacarons_Click(sender : UIButton) {
        let objViewController : frmPreOrderItemList = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderItemList") as! frmPreOrderItemList
        self.present(objViewController, animated: true, completion: nil)
        objViewController.DataBind(pPreOrderSalesHeader : PreOrderSalesHeader, pstrItemType : "Macaron")
    }
    
    @IBAction func cmdMainMenuCholoate_Click(sender : UIButton) {
        let objViewController : frmPreOrderItemList = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderItemList") as! frmPreOrderItemList
        self.present(objViewController, animated: true, completion: nil)
        objViewController.DataBind(pPreOrderSalesHeader : PreOrderSalesHeader, pstrItemType : "Choloate")
    }
}
