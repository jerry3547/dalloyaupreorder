//
//  frmPreOrderDeliveryDateTime.swift
//  DalloyauPreOrder
//
//  Created by jerry on 27/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderDeliveryDateTime : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    // Declare //
    var DeliveryDateTimeList : frmPreOrderDeliveryDateTimeCell.DeliveryDateTimeStructList = frmPreOrderDeliveryDateTimeCell.DeliveryDateTimeStructList()
    
    // Interface //
    @IBOutlet var CollectionViewStore : UICollectionView!
    
    @IBOutlet var ViewNavigationBarHeader : UIView!
    var NavigationBarHeader : frmPreOrderNavigationHeader = frmPreOrderNavigationHeader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CollectionViewStore.dataSource = self
        CollectionViewStore.delegate = self
        CollectionViewStore.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is frmPreOrderNavigationHeader
        {
            NavigationBarHeader = segue.destination as! frmPreOrderNavigationHeader
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DeliveryDateTimeList.Detail.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objCollectionViewCell : frmPreOrderDeliveryDateTimeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateTime", for: indexPath) as! frmPreOrderDeliveryDateTimeCell
        objCollectionViewCell.DataBind(pDeliveryDateTime : DeliveryDateTimeList.Detail[indexPath.row])
        return objCollectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objCollectionViewCell : frmPreOrderDeliveryDateTimeCell = collectionView.cellForItem(at: indexPath) as! frmPreOrderDeliveryDateTimeCell
        objCollectionViewCell.Selected()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let objCollectionViewCell : frmPreOrderDeliveryDateTimeCell = collectionView.cellForItem(at: indexPath) as! frmPreOrderDeliveryDateTimeCell
        objCollectionViewCell.Deselected()
    }
    
    func ContainerViewSystemMenuShow(pbitIsHidden : Bool) {
        if (pbitIsHidden == true)
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 50)
        }
        else
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        }
    }
    
    @IBAction func cmdPreOrderBack_Click(sender : UIButton){
        let objViewController : frmPreOrderDeliveryMethod = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderDeliveryMethod") as! frmPreOrderDeliveryMethod
        self.present(objViewController, animated: false, completion: nil)
    }
    
    @IBAction func cmdPreOrderNext_Click(sender : UIButton){
    }
}

class frmPreOrderDeliveryDateTimeCell : RoundUICollectionViewCell
{
    @IBOutlet var LableMonth : RoundUILabel!
    @IBOutlet var LableDay : RoundUILabel!
    @IBOutlet var LableTime : RoundUILabel!
    
    func DataBind(pDeliveryDateTime : DeliveryDateTimeStructList.DeliveryDateTimeStruct) {
        LableMonth.text = pDeliveryDateTime.strMonth
        LableDay.text = pDeliveryDateTime.strDay
        LableTime.text = pDeliveryDateTime.strTime
    }
    
    func Selected(){
        borderColor = UIColor(red: 0/255, green: 74/255, blue: 212/255, alpha: 1.0)
        LableMonth.borderColor = UIColor(red: 0/255, green: 74/255, blue: 212/255, alpha: 1.0)
        LableDay.borderColor = UIColor(red: 0/255, green: 74/255, blue: 212/255, alpha: 1.0)
        LableMonth.textColor = UIColor(red: 42/255, green: 42/255, blue: 48/255, alpha: 1.0)
        LableDay.textColor = UIColor(red: 42/255, green: 42/255, blue: 48/255, alpha: 1.0)
        LableTime.textColor = UIColor(red: 42/255, green: 42/255, blue: 48/255, alpha: 1.0)
    }
    
    func Deselected(){
        borderColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
        LableMonth.borderColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
        LableDay.borderColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
        LableMonth.textColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
        LableDay.textColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
        LableTime.textColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
    }
    
    public struct DeliveryDateTimeStructList {
        var Detail : [DeliveryDateTimeStruct] = []
        
        init(){
            for intDay : Int in 1...14
            {
                var DeliveryDateTime : DeliveryDateTimeStruct = DeliveryDateTimeStruct()
                let dtmCurrentDate : Date = Date()
                
                let objMonthFormatter : DateFormatter = DateFormatter()
                objMonthFormatter.dateFormat = "MMM"
                
                let objDayFormatter : DateFormatter = DateFormatter()
                objDayFormatter.dateFormat = "d"
                
                var dateComponent = DateComponents()
                dateComponent.day = intDay + (Calendar.current.component(.day, from: dtmCurrentDate)) * -1
                
                DeliveryDateTime.strMonth = objMonthFormatter.string(from : Calendar.current.date(byAdding: dateComponent, to: dtmCurrentDate)!)
                DeliveryDateTime.strDay = objDayFormatter.string(from : Calendar.current.date(byAdding: dateComponent, to: dtmCurrentDate)!)
                DeliveryDateTime.strTime = "9:00 am - 1:00 pm"
                Detail.append(DeliveryDateTime)
                
                DeliveryDateTime.strMonth = objMonthFormatter.string(from : Calendar.current.date(byAdding: dateComponent, to: dtmCurrentDate)!)
                DeliveryDateTime.strDay = objDayFormatter.string(from : Calendar.current.date(byAdding: dateComponent, to: dtmCurrentDate)!)
                DeliveryDateTime.strTime = "1:00 pm - 6:00 pm"
                Detail.append(DeliveryDateTime)
            }
        }
        
        struct DeliveryDateTimeStruct
        {
            var strMonth : String = ""
            var strDay : String = ""
            var strTime : String = ""
        }
        
    }
}
