//
//  frmPreOrderItemList.swift
//  DalloyauPreOrder
//
//  Created by jerry on 26/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderItemList : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    var PreOrderItemStructList : StructMaster.PreOrder.PreOrderItemStructList = StructMaster.PreOrder.PreOrderItemStructList()
    var PreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct = StructMaster.PreOrder.PreOrderSalesHeaderStruct()
    
    // Navigation //
    @IBOutlet var ViewNavigationBarHeader : UIView!
    @IBOutlet var ViewNavigationBarFooter : UIView!
    var NavigationBarHeader : frmPreOrderNavigationHeader = frmPreOrderNavigationHeader()
    var NavigationBarFooter : frmPreOrderNavigationFooter = frmPreOrderNavigationFooter()
    
    // Interface //
    @IBOutlet var CollectionViewItemList : UICollectionView!
    @IBOutlet var ContainerViewItemDetailPopUp : UIView!
    var ViewControllerItemDetailPopUp : frmPreOrderItemDetail = frmPreOrderItemDetail()
    @IBOutlet var ContainerViewItemCakePopUp : UIView!
    var ViewControllerItemCakePopUp : frmPreOrderItemCake = frmPreOrderItemCake()
    @IBOutlet var ContainerViewItemGiftBoxPopUp : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CollectionViewItemList.dataSource = self
        CollectionViewItemList.delegate = self
        
        ContainerViewItemDetailPopUp.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        ContainerViewItemCakePopUp.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        ContainerViewItemGiftBoxPopUp.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        
        ContainerViewItemDetailPopUp.isHidden = true
        ContainerViewItemCakePopUp.isHidden = true
        ContainerViewItemGiftBoxPopUp.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is frmPreOrderNavigationHeader
        {
            NavigationBarHeader = segue.destination as! frmPreOrderNavigationHeader
        }
        if segue.destination is frmPreOrderNavigationFooter
        {
            NavigationBarFooter = segue.destination as! frmPreOrderNavigationFooter
        }
        if segue.destination is frmPreOrderItemDetail
        {
            ViewControllerItemDetailPopUp = segue.destination as! frmPreOrderItemDetail
        }
        if segue.destination is frmPreOrderItemCake
        {
            ViewControllerItemCakePopUp = segue.destination as! frmPreOrderItemCake
        }
    }
    
    func DataBind(pPreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct, pstrItemType : String) {
        PreOrderItemStructList.Read(pstrItemType: pstrItemType)
        PreOrderSalesHeader = pPreOrderSalesHeader
        NavigationBarHeader.DataBind(pPreOrderSalesHeader: PreOrderSalesHeader)
        NavigationBarFooter.DataBind(pPreOrderSalesHeader: PreOrderSalesHeader)
    }
    
    func ContainerViewSystemMenuShow(pbitIsHidden : Bool) {
        if (pbitIsHidden == true)
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 50)
        }
        else
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PreOrderItemStructList.Detail.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objCollectionViewCell : frmPreOrderItemListCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Item", for: indexPath) as! frmPreOrderItemListCell
        let PreOrderItem : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct = PreOrderItemStructList.Detail[indexPath.row]
        let decSalesQty : Double = PreOrderSalesHeader.GetSalesQty(pstrItemType: PreOrderItem.strItemType, pstrItemCode: PreOrderItem.strTypeCode)
        objCollectionViewCell.DataBind(PreOrderItem : PreOrderItem, pdecSalesQty : decSalesQty)
        return objCollectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch (PreOrderItemStructList.Detail[indexPath.row].strItemType) {
        case "Cake":
            let PreOrderItem : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct = PreOrderItemStructList.Detail[indexPath.row]
            var tmpPreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct = PreOrderSalesHeader
            let intRowIndex : Int = tmpPreOrderSalesHeader.AddNewDetail(pstrItemType: PreOrderItem.strItemType, pstrItemCode: PreOrderItem.strTypeCode)
            ViewControllerItemCakePopUp.DataBind(pPreOrderSalesHeader: tmpPreOrderSalesHeader, pintRowIndex: intRowIndex, pPreOrderItem: PreOrderItem)
            ContainerViewItemCakePopUp.isHidden = false
            break
        case "Pastrie", "Canape", "Festive":
            let PreOrderItem : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct = PreOrderItemStructList.Detail[indexPath.row]
            var tmpPreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct = PreOrderSalesHeader
            let intRowIndex : Int = tmpPreOrderSalesHeader.AddNewDetail(pstrItemType: PreOrderItem.strItemType, pstrItemCode: PreOrderItem.strTypeCode)
            ViewControllerItemDetailPopUp.DataBind(pPreOrderSalesHeader: tmpPreOrderSalesHeader, pintRowIndex: intRowIndex, pPreOrderItem: PreOrderItem)
            ContainerViewItemDetailPopUp.isHidden = false
            break
        case "Macaron", "Cholocate":
            break
        default:
            break
        }
    }
    
    func frmPreOrderItemDetail_Close() {
        ContainerViewItemDetailPopUp.isHidden = true
        ContainerViewItemCakePopUp.isHidden = true
        ContainerViewItemGiftBoxPopUp.isHidden = true
    }
    
    func frmPreOrderItemDetail_Save(pPreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct, pstrItemType : String) {
        ContainerViewItemDetailPopUp.isHidden = true
        ContainerViewItemCakePopUp.isHidden = true
        ContainerViewItemGiftBoxPopUp.isHidden = true
        DataBind(pPreOrderSalesHeader : pPreOrderSalesHeader, pstrItemType : pstrItemType)
        CollectionViewItemList.reloadData()
    }
}

class frmPreOrderItemListCell : RoundUICollectionViewCell
{
    @IBOutlet var ImageViewItemImage : RoundUIImageView!
    @IBOutlet var LabelItemDesc : RoundUILabel!
    @IBOutlet var LabelItemSalesAmount : RoundUILabel!
    @IBOutlet var LabelItemDeliveryDateCaption : RoundUILabel!
    @IBOutlet var LabelItemDeliveryDate : RoundUILabel!
    @IBOutlet var ImageViewPreOrder : RoundUIImageView!
    @IBOutlet var LabelItemPreOrderQty : RoundUILabel!
    
    func DataBind(PreOrderItem : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct, pdecSalesQty : Double) {
        ImageViewItemImage.borderWidth  = 0
        LabelItemDesc.borderWidth  = 0
        LabelItemSalesAmount.borderWidth  = 0
        LabelItemDeliveryDateCaption.borderWidth  = 0
        LabelItemDeliveryDate.borderWidth  = 0
        
        var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
        tblPOSItemImageList.Read(pstrTypeCode: PreOrderItem.strTypeCode)
        ImageViewItemImage.image = tblPOSItemImageList.Detail.first?.GetUIImage()
        LabelItemDesc.text = PreOrderItem.strTypeDesc
        LabelItemSalesAmount.text = GetAmtRangeString(pAmtFm: PreOrderItem.decSalesAmtFm, pAmtTo: PreOrderItem.decSalesAmtTo)
        LabelItemDeliveryDate.text = GetDeliveryDateString(pintDeliveryDay : PreOrderItem.intDeliveryDayFm)
        
        if (pdecSalesQty == 0)
        {
            ImageViewPreOrder.isHidden = true
            LabelItemPreOrderQty.isHidden = true
        }
        else
        {
            ImageViewPreOrder.isHidden = false
            LabelItemPreOrderQty.isHidden = false
            LabelItemPreOrderQty.text = GetQtyString(pQty: pdecSalesQty)
        }
    }
}
