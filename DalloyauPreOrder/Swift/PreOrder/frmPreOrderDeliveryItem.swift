//
//  frmPreOrderDeliveryItem.swift
//  DalloyauPreOrder
//
//  Created by jerry on 27/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderDeliveryItem : UIViewController
{
    @IBOutlet var ViewNavigationBarHeader : UIView!
    var NavigationBarHeader : frmPreOrderNavigationHeader = frmPreOrderNavigationHeader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is frmPreOrderNavigationHeader
        {
            NavigationBarHeader = segue.destination as! frmPreOrderNavigationHeader
        }
    }
    
    func ContainerViewSystemMenuShow(pbitIsHidden : Bool) {
        if (pbitIsHidden == true)
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 50)
        }
        else
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        }
    }
    
    @IBAction func cmdPreOrderBack_Click(sender : UIButton){
        let objViewController : frmPreOrderCustInfo = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderCustInfo") as! frmPreOrderCustInfo
        self.present(objViewController, animated: false, completion: nil)
    }
    
    @IBAction func cmdPreOrderNext_Click(sender : UIButton){
    }
}
