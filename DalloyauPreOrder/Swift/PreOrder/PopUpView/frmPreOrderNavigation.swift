//
//  frmPreOrderNavigation.swift
//  DalloyauPreOrder
//
//  Created by jerry on 26/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderNavigationHeader : UIViewController
{
    private var strAdminPassword : String = ""
    private var bitIsHidden : Bool = true
    private var PreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct = StructMaster.PreOrder.PreOrderSalesHeaderStruct()
    
    // Interface //
    @IBOutlet var LabelStoreDesc : UILabel!
    @IBOutlet var LabelPreOrderDocNo : UILabel!
    
    @IBOutlet var ViewPassword : UIView!
    @IBOutlet var ViewSystemMenu : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ViewPassword.frame = CGRect(x: 724, y: 40, width: 300, height: 400)
        ViewSystemMenu.frame = CGRect(x: 720, y: 40, width: 300, height: 400)
        
        ViewPassword.isHidden = true
        ViewSystemMenu.isHidden = true
    }
    
    func DataBind(pPreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct) {
        self.PreOrderSalesHeader = pPreOrderSalesHeader
        LabelStoreDesc.text = "\(PreOrderSalesHeader.strStoreCode) \(PreOrderSalesHeader.strStoreDesc)"
        LabelPreOrderDocNo.text = "\(PreOrderSalesHeader.strPreOrderDocNo)"
    }
    
    func UpdateIsHidden(pbitIsHidden : Bool) -> Bool {
        if (pbitIsHidden)
        {
            bitIsHidden = false
            ViewPassword.isHidden = false
            ViewSystemMenu.isHidden = true
        }
        else
        {
            bitIsHidden = true
            ViewPassword.isHidden = true
            ViewSystemMenu.isHidden = true
        }
        
        return bitIsHidden
    }
    
    @IBAction func cmdPasswordKeyPad_Click(sender : UIButton) {
        switch(sender.titleLabel!.text)
        {
        case "0","1","2","3","4","5","6","7","8","9":
            strAdminPassword = strAdminPassword + sender.titleLabel!.text!
            break
        case"C":
            strAdminPassword = ""
            break
        case "Enter":
            if(strAdminPassword == "1234")
            {
                ViewPassword.isHidden = true
                ViewSystemMenu.isHidden = false
            }
            break
        default:
            break
        }
    }
    
    @IBAction func cmdSetting_Click(sender : UIButton) {
        strAdminPassword = ""
        if let objParentViewController = self.parent as? frmPreOrderMainMenu
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : bitIsHidden))
        }
        else if let objParentViewController = self.parent as? frmPreOrderItemList
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : bitIsHidden))
        }
        else if let objParentViewController = self.parent as? frmPreOrderSummary
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : bitIsHidden))
        }
        else if let objParentViewController = self.parent as? frmPreOrderCustInfo
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : bitIsHidden))
        }
        else if let objParentViewController = self.parent as? frmPreOrderDeliveryMethod
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : bitIsHidden))
        }
        else if let objParentViewController = self.parent as? frmPreOrderDeliveryItem
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : bitIsHidden))
        }
        else if let objParentViewController = self.parent as? frmPreOrderDeliveryStore
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : bitIsHidden))
        }
        else if let objParentViewController = self.parent as? frmPreOrderDeliveryAddress
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : bitIsHidden))
        }
        else if let objParentViewController = self.parent as? frmPreOrderDeliveryRecipient
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : bitIsHidden))
        }
        else if let objParentViewController = self.parent as? frmPreOrderDeliveryDateTime
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : bitIsHidden))
        }
    }
    
    @IBAction func cmdSystemReset_Click(sender : UIButton) {
        let objViewController : frmPreOrderMainMenu = UIStoryboard(name: "frmPreOrderMainMenu", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderMainMenu") as! frmPreOrderMainMenu
        self.present(objViewController, animated: false, completion: nil)
    }
    
    @IBAction func cmdSystemDebug_Click(sender : UIButton) {
        let objViewController : frmSystemMaintenanceTabBarController = UIStoryboard(name: "SystemMaintenance", bundle: nil).instantiateViewController(withIdentifier: "frmSystemMaintenanceTabBarController") as! frmSystemMaintenanceTabBarController
        self.present(objViewController, animated: false, completion: nil)
    }
    
    @IBAction func cmdExit_Click(sender : UIButton) {
        if let objParentViewController = self.parent as? frmPreOrderMainMenu
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : false))
        }
        else if let objParentViewController = self.parent as? frmPreOrderItemList
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : false))
        }
        else if let objParentViewController = self.parent as? frmPreOrderSummary
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : false))
        }
        else if let objParentViewController = self.parent as? frmPreOrderCustInfo
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : false))
        }
        else if let objParentViewController = self.parent as? frmPreOrderDeliveryMethod
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : false))
        }
        else if let objParentViewController = self.parent as? frmPreOrderDeliveryItem
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : false))
        }
        else if let objParentViewController = self.parent as? frmPreOrderDeliveryStore
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : false))
        }
        else if let objParentViewController = self.parent as? frmPreOrderDeliveryAddress
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : false))
        }
        else if let objParentViewController = self.parent as? frmPreOrderDeliveryRecipient
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : false))
        }
        else if let objParentViewController = self.parent as? frmPreOrderDeliveryDateTime
        {
            objParentViewController.ContainerViewSystemMenuShow(pbitIsHidden : UpdateIsHidden(pbitIsHidden : false))
        }
    }
}

class frmPreOrderNavigationFooter : UIViewController
{
    private var PreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct = StructMaster.PreOrder.PreOrderSalesHeaderStruct()

    @IBOutlet var ImageViewPreOrder : RoundUIImageView!
    @IBOutlet var LabelItemPreOrderQty : RoundUILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func DataBind(pPreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct) {
        self.PreOrderSalesHeader = pPreOrderSalesHeader
        let decSalesQty : Double = PreOrderSalesHeader.GetSalesQty()
        if (decSalesQty == 0.0) {
            ImageViewPreOrder.isHidden = true
            LabelItemPreOrderQty.isHidden = true
        }
        else {
            ImageViewPreOrder.isHidden = false
            LabelItemPreOrderQty.isHidden = false
            LabelItemPreOrderQty.text = GetQtyString(pQty: decSalesQty)
        }
    }
    
    @IBAction func cmdHome_Click(sender : UIButton) {
        let objViewController : frmPreOrderMainMenu = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderMainMenu") as! frmPreOrderMainMenu
        self.present(objViewController, animated: false, completion: nil)
        objViewController.DataBind(pPreOrderSalesHeader: PreOrderSalesHeader)
    }
    
    @IBAction func cmdCheckOut_Click(sender : UIButton) {
        if (PreOrderSalesHeader.SalesDetailList.count == 0)
        {
            let alertController = UIAlertController(title :"Shopping Cart", message:"請選擇產品", preferredStyle: UIAlertController.Style.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
        let objViewController : frmPreOrderSummary = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderSummary") as! frmPreOrderSummary
        self.present(objViewController, animated: true, completion: nil)
        objViewController.DataBind(pPreOrderSalesHeader: PreOrderSalesHeader)
        }
    }
}
