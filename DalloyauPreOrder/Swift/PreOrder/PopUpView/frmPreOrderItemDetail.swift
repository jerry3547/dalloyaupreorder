//
//  frmPreOrderItemDetail.swift
//  DalloyauPreOrder
//
//  Created by jerry on 26/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderItemDetail : UIViewController
{
    private var PreOrderItem : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct = StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct()
    private var PreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct = StructMaster.PreOrder.PreOrderSalesHeaderStruct()
    private var intRowIndex : Int = 0
    private var decSalesQty : Double = 0.0
    
    // Interface //
    @IBOutlet var LabelLineNo : RoundUILabel!
    @IBOutlet var ImageViewItemImage : RoundUIImageView!
    @IBOutlet var LabelItemDesc : RoundUILabel!
    @IBOutlet var LabelItemRemark : RoundUITextView!
    @IBOutlet var LabelItemSalesUnitPrice : RoundUILabel!
    @IBOutlet var LabelItemSalesAmt : RoundUILabel!
    @IBOutlet var LabelItemSalesQty: RoundUILabel!
    @IBOutlet var LabelItemDeliveryDate : RoundUILabel!
    
    @IBOutlet var ButtonSave : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func DataBind(pPreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct, pintRowIndex : Int, pPreOrderItem : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct) {
        PreOrderItem = pPreOrderItem
        intRowIndex = pintRowIndex
        PreOrderSalesHeader = pPreOrderSalesHeader
        decSalesQty = PreOrderSalesHeader.SalesDetailList[intRowIndex].decSalesQty
        
        var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
        tblPOSItemImageList.Read(pstrTypeCode: PreOrderItem.strTypeCode)
        ImageViewItemImage.image = tblPOSItemImageList.Detail.first?.GetUIImage()
        LabelLineNo.text = "#\(intRowIndex+1) - \(PreOrderItem.strTypeCode)"
        LabelItemDesc.text = PreOrderItem.strTypeDesc
        LabelItemRemark.text = PreOrderItem.strTypeRemark
        LabelItemSalesUnitPrice.text = GetAmtRangeString(pAmtFm: PreOrderItem.decSalesAmtFm, pAmtTo: PreOrderItem.decSalesAmtTo)
        LabelItemDeliveryDate.text = "Earliest Pick up \(GetDeliveryDateString(pintDeliveryDay : PreOrderItem.intDeliveryDayFm))"
        LabelItemSalesQty.text = GetQtyString(pQty: decSalesQty)
        LabelItemSalesAmt.text = GetAmtString(pAmt: decSalesQty * PreOrderItem.decSalesAmtFm)
    }
    
    @IBAction func cmdButtonNumber_Click(sender : UIButton) {
        let strInput : NSString = sender.titleLabel!.text! as NSString
        switch(strInput)
        {
        case "0","1","2","3","4","5","6","7","8","9":
            let decInput : Double = strInput.doubleValue
            decSalesQty = decSalesQty * 10.0 + decInput
            break
        case "C":
            decSalesQty = 0
            break
        default:
            break
        }
        
        LabelItemSalesQty.text = GetQtyString(pQty: decSalesQty)
        LabelItemSalesAmt.text = GetAmtString(pAmt: decSalesQty * PreOrderItem.decSalesAmtFm)
    }
    
    @IBAction func cmdItemClose_Click(sender : UIButton){
        if let frmPreOrderItemList = self.parent as? frmPreOrderItemList
        {
            frmPreOrderItemList.frmPreOrderItemDetail_Close()
        }
    }
    
    @IBAction func cmdItemSave_Click(sender : UIButton){
        PreOrderSalesHeader.SalesDetailList[intRowIndex].decSalesQty = decSalesQty
        PreOrderSalesHeader.SalesDetailList[intRowIndex].decSalesAmt = PreOrderSalesHeader.SalesDetailList[intRowIndex].decUnitPrice * decSalesQty
        PreOrderSalesHeader.Update()
        if let frmPreOrderItemList = self.parent as? frmPreOrderItemList
        {
            frmPreOrderItemList.frmPreOrderItemDetail_Save(pPreOrderSalesHeader : PreOrderSalesHeader, pstrItemType: PreOrderItem.strItemType)
        }
    }
}

extension String {
    func toDouble() -> Double {
        let nsString = self as NSString
        return nsString.doubleValue
    }
}
