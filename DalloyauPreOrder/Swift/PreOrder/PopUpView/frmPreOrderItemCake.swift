//
//  frmPreOrderItemCake.swift
//  DalloyauPreOrder
//
//  Created by jerry on 26/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderItemCake : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    private var PreOrderItemCake : StructMaster.PreOrder.PreOrderItemCakeStructList.PreOrderItemCakeStruct = StructMaster.PreOrder.PreOrderItemCakeStructList.PreOrderItemCakeStruct()
    private var PreOrderCakeLollipopList : StructMaster.PreOrder.PreOrderItemStructList = StructMaster.PreOrder.PreOrderItemStructList()
    private var PreOrderCakeLollipopMessageList : StructMaster.PreOrder.PreOrderItemCakeMessageStructList = StructMaster.PreOrder.PreOrderItemCakeMessageStructList()
    private var PreOrderCakeLollipopMessagePath : [String] = []
    private var PreOrderCakeTopperList : StructMaster.PreOrder.PreOrderItemStructList = StructMaster.PreOrder.PreOrderItemStructList()
    
    private var CakeAttributePrint : [String] = []
    private var CakeAttributeFlavor : [String] = []
    private var CakeAttributeSize : [String] = []
    private var PreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct = StructMaster.PreOrder.PreOrderSalesHeaderStruct()
    private var PreOrderSalesDetail : StructMaster.PreOrder.PreOrderSalesDetailStruct = StructMaster.PreOrder.PreOrderSalesDetailStruct()
    private var intRowIndex : Int = 0
    private var decSalesQty : Double = 0.0
    private var strDetailStep : String = ""
    
    @IBOutlet var ButtonBack : UIButton!
    @IBOutlet var ButtonNext : UIButton!
    
    // Interface //
    @IBOutlet var LabelLineNo : RoundUILabel!
    @IBOutlet var ImageViewItemImage : RoundUIImageView!
    @IBOutlet var LabelItemDesc : RoundUILabel!
    @IBOutlet var LabelItemAttributes : RoundUILabel!
    @IBOutlet var LabelItemRemark : RoundUITextView!
    @IBOutlet var LabelItemSalesUnitPrice : RoundUILabel!
    @IBOutlet var LabelItemDeliveryDate : RoundUILabel!
    
    // Interface - Attribute //
    @IBOutlet var ScrollViewAttribute : UIScrollView!
    @IBOutlet var CollectionViewAttributePrintView : UIView!
    @IBOutlet var CollectionViewAttributePrintCaption : UILabel!
    @IBOutlet var CollectionViewAttributePrint : UICollectionView!
    @IBOutlet var CollectionViewAttributeFlavorView : UIView!
    @IBOutlet var CollectionViewAttributeFlavorCaption : UILabel!
    @IBOutlet var CollectionViewAttributeFlavor : UICollectionView!
    @IBOutlet var CollectionViewAttributeSizeView : UIView!
    @IBOutlet var CollectionViewAttributeSizeCaption : UILabel!
    @IBOutlet var CollectionViewAttributeSize : UICollectionView!
    
    // Interface - Lollipop //
    @IBOutlet var ViewLollipop : UIView!
    @IBOutlet var CollectionViewLollipop : UICollectionView!
    
    // Interface - Message //
    @IBOutlet var ViewMessage : UIView!
    @IBOutlet var CollectionViewMessage : UICollectionView!
    
    // Interface - Topper //
    @IBOutlet var ViewTopper : UIView!
    @IBOutlet var CollectionViewTopper : UICollectionView!
    
    // Interface - AddOn //
    @IBOutlet var CollectionViewAddOn : UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CollectionViewAttributePrint.dataSource = self
        CollectionViewAttributePrint.delegate = self
        
        CollectionViewAttributeFlavor.dataSource = self
        CollectionViewAttributeFlavor.delegate = self
        
        CollectionViewAttributeSize.dataSource = self
        CollectionViewAttributeSize.delegate = self
        
        CollectionViewLollipop.dataSource = self
        CollectionViewLollipop.delegate = self
        
        CollectionViewMessage.dataSource = self
        CollectionViewMessage.delegate = self
        
        CollectionViewTopper.dataSource = self
        CollectionViewTopper.delegate = self
        
        CollectionViewAddOn.dataSource = self
        CollectionViewAddOn.delegate = self
        
        ScrollViewAttribute.frame = CGRect(x: 565, y: 5, width: 350, height: 587)
        ViewLollipop.frame = CGRect(x: 565, y: 5, width: 350, height: 587)
        ViewMessage.frame = CGRect(x: 565, y: 5, width: 350, height: 587)
        ViewTopper.frame = CGRect(x: 565, y: 5, width: 350, height: 587)
    }
    
    func DataBind(pPreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct, pintRowIndex : Int, pPreOrderItem : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct) {
        intRowIndex = pintRowIndex
        print("frmPreOrderItemCake.DataBind (Index : \(pintRowIndex), Type : Cake, Item : \(pPreOrderItem.strTypeCode))")
        PreOrderItemCake.Read(pstrTypeCode : pPreOrderItem.strTypeCode)
        PreOrderSalesHeader = pPreOrderSalesHeader
        PreOrderSalesDetail = pPreOrderSalesHeader.SalesDetailList[intRowIndex]
        strDetailStep = "Attribute"
        PreOrderCakeLollipopList.Read(pstrItemType: "Lollipop")
        var PreOrderItemDetail : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct = StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct()
        PreOrderItemDetail.strItemType = "Lollipop"
        PreOrderItemDetail.strTypeCode = "N/A"
        PreOrderCakeLollipopList.Detail.append(PreOrderItemDetail)
        PreOrderCakeLollipopMessageList.Read(pstrParentID : "")
        PreOrderCakeLollipopMessagePath = [""]
        PreOrderCakeTopperList.Read(pstrItemType: "Topper")
        
        // Reset Interface //
        CollectionViewAttributePrint.DeselectAll()
        CollectionViewAttributePrint.reloadData()
        CollectionViewAttributeFlavor.DeselectAll()
        CollectionViewAttributeFlavor.reloadData()
        CollectionViewAttributeSize.DeselectAll()
        CollectionViewAttributeSize.reloadData()
        CollectionViewLollipop.DeselectAll()
        CollectionViewLollipop.reloadData()
        CollectionViewMessage.DeselectAll()
        CollectionViewMessage.reloadData()
        CollectionViewTopper.DeselectAll()
        CollectionViewTopper.reloadData()
        CollectionViewAddOn.DeselectAll()
        CollectionViewAddOn.reloadData()
        
        ScrollViewAttribute.isHidden = false
        ViewLollipop.isHidden = true
        ViewMessage.isHidden = true
        ViewTopper.isHidden = true
        
        // Main //
        var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
        tblPOSItemImageList.Read(pstrTypeCode: PreOrderItemCake.strTypeCode)
        ImageViewItemImage.image = tblPOSItemImageList.Detail.first?.GetUIImage()
        LabelLineNo.text = (PreOrderItemCake.intDetailCount==1 ? "#\(intRowIndex+1) - \(PreOrderItemCake.CakeItemList.first!.strItemCode)" : "#\(intRowIndex+1) - select item (\(PreOrderItemCake.intDetailCount))")
        LabelItemDesc.text = PreOrderItemCake.strTypeDesc
        LabelItemRemark.text = PreOrderItemCake.strTypeRemark
        LabelItemSalesUnitPrice.text = GetAmtRangeString(pAmtFm: PreOrderItemCake.decSalesAmtFm, pAmtTo: PreOrderItemCake.decSalesAmtTo)
        LabelItemDeliveryDate.text = "Earliest Pick up \(GetDeliveryDateString(pintDeliveryDay : PreOrderItemCake.intDeliveryDayFm))"
    
        var intLocationY : Int = 0
        // Attribute - Print //
        CakeAttributePrint = PreOrderItemCake.GetCakeAttribute(pstrAttributeType: "Print")
        if (CakeAttributePrint.count > 0) {
            let intRowCount : Int = Int(ceil(Double(CakeAttributePrint.count) / 2.0))
            CollectionViewAttributePrint.DeselectAll()
            CollectionViewAttributePrint.reloadData()
            CollectionViewAttributePrint.frame = CGRect(x: 5, y: 25, width: 340, height: 80 * intRowCount)
            CollectionViewAttributePrintView.frame = CGRect(x: 0, y: intLocationY, width: 350, height: 80 * intRowCount + 20)
            CollectionViewAttributePrintView.isHidden = false
            intLocationY = intLocationY + 80 * intRowCount + 30
        }
        else {
            CollectionViewAttributePrintView.isHidden = true
        }
            // Attribute - Flavor //
        CakeAttributeFlavor = PreOrderItemCake.GetCakeAttribute(pstrAttributeType: "Flavor")
        if (CakeAttributeFlavor.count > 0) {
            let intRowCount : Int = Int(ceil(Double(CakeAttributeFlavor.count) / 2.0))
            CollectionViewAttributeFlavor.DeselectAll()
            CollectionViewAttributeFlavor.reloadData()
            CollectionViewAttributeFlavor.frame = CGRect(x: 5, y: 25, width: 340, height: 80 * intRowCount)
            CollectionViewAttributeFlavorView.frame = CGRect(x: 0, y: intLocationY, width: 350, height: 80 * intRowCount + 20)
            CollectionViewAttributeFlavorView.isHidden = false
            intLocationY = intLocationY + 80 * intRowCount + 30
        }
        else {
            CollectionViewAttributeFlavorView.isHidden = true
        }
        
        // Attribute - Size //
        CakeAttributeSize = PreOrderItemCake.GetCakeAttribute(pstrAttributeType: "Size")
        if (CakeAttributeSize.count > 0) {
            let intRowCount : Int = Int(ceil(Double(CakeAttributeSize.count) / 2.0))
            CollectionViewAttributeSize.DeselectAll()
            CollectionViewAttributeSize.reloadData()
            CollectionViewAttributeSize.frame = CGRect(x: 5, y: 25, width: 340, height: 80 * intRowCount)
            CollectionViewAttributeSizeView.frame = CGRect(x: 0, y: intLocationY, width: 350, height: 80 * intRowCount + 20)
            CollectionViewAttributeSizeView.isHidden = false
            intLocationY = intLocationY + 60 * intRowCount + 500
        }
        else {
            CollectionViewAttributeSizeView.isHidden = true
        }
        ScrollViewAttribute.contentSize = CGSize(width: 350, height: intLocationY)
        ScrollViewAttribute.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
    }
    
    func DataBind_LabelItemAttributes() {
        var pstrAttributePrint : String = ""
        var pstrAttributeFlavor : String = ""
        var pstrAttributeSize : String = ""
        LabelItemAttributes.text = ""
        if (CollectionViewAttributePrintView.isHidden == false && CollectionViewAttributePrint.indexPathsForSelectedItems!.count == 1 )
        {
            let SelectedRow : Int = CollectionViewAttributePrint.indexPathsForSelectedItems!.first!.row
            pstrAttributePrint = "\(CakeAttributePrint[SelectedRow])"
            LabelItemAttributes.text = LabelItemAttributes.text! + "\(CakeAttributePrint[SelectedRow]), "
        }
        if (CollectionViewAttributeFlavorView.isHidden == false && CollectionViewAttributeFlavor.indexPathsForSelectedItems!.count == 1 )
        {
            let SelectedRow : Int = CollectionViewAttributeFlavor.indexPathsForSelectedItems!.first!.row
            pstrAttributeFlavor = "\(CakeAttributeFlavor[SelectedRow])"
            LabelItemAttributes.text = LabelItemAttributes.text! + "\(CakeAttributeFlavor[SelectedRow]), "
        }
        if (CollectionViewAttributeSizeView.isHidden == false && CollectionViewAttributeSize.indexPathsForSelectedItems!.count == 1 )
        {
            let SelectedRow : Int = CollectionViewAttributeSize.indexPathsForSelectedItems!.first!.row
            pstrAttributeSize = "\(CakeAttributeSize[SelectedRow])"
            LabelItemAttributes.text = LabelItemAttributes.text! + "\(CakeAttributeSize[SelectedRow]), "
        }
        
        PreOrderItemCake.GetCakeItemList(pstrAttributePrint: pstrAttributePrint, pstrAttributeFlavor: pstrAttributeFlavor, pstrAttributeSize: pstrAttributeSize)
        LabelLineNo.text = (PreOrderItemCake.intDetailCount==1 ? "#\(intRowIndex+1) - \(PreOrderItemCake.CakeItemList.first!.strItemCode)" : "#\(intRowIndex+1) - select item (\(PreOrderItemCake.intDetailCount))")
        LabelItemSalesUnitPrice.text = GetAmtRangeString(pAmtFm: PreOrderItemCake.decSalesAmtFm, pAmtTo: PreOrderItemCake.decSalesAmtTo)
        LabelItemDeliveryDate.text = "Earliest Pick up \(GetDeliveryDateString(pintDeliveryDay : PreOrderItemCake.intDeliveryDayFm))"
    }
    
    func DataBind_Lollipop(pAddOnDetail : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct) {
        var PreOrderAddOnLollipop : StructMaster.PreOrder.PreOrderSalesDetailAddOnStruct = StructMaster.PreOrder.PreOrderSalesDetailAddOnStruct()
        PreOrderAddOnLollipop.intLineNo = PreOrderSalesHeader.SalesDetailList[intRowIndex].intLineNo
        PreOrderAddOnLollipop.intAddOnLineNo = (PreOrderSalesDetail.AddOnDetailList.count == 0 ? 1 : PreOrderSalesDetail.AddOnDetailList.last!.intAddOnLineNo+1)
        PreOrderAddOnLollipop.strItemType = pAddOnDetail.strItemType
        PreOrderAddOnLollipop.strItemCode = pAddOnDetail.strTypeCode
        PreOrderAddOnLollipop.strCustRemark = ""
        PreOrderAddOnLollipop.decUnitPrice = pAddOnDetail.decSalesAmtFm
        PreOrderAddOnLollipop.decSalesQty = 1
        PreOrderAddOnLollipop.decSalesAmt = pAddOnDetail.decSalesAmtFm
        if (PreOrderSalesDetail.AddOnDetailList.count == 0)
        {
            PreOrderSalesDetail.AddOnDetailList.append(PreOrderAddOnLollipop)
        }
        else
        {
            PreOrderSalesDetail.AddOnDetailList[0] = PreOrderAddOnLollipop
        }
        CollectionViewAddOn.reloadData()
    }
    
    func DataBind_Topper(pAddOnDetail : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct) {
        var PreOrderAddOnLollipop : StructMaster.PreOrder.PreOrderSalesDetailAddOnStruct = StructMaster.PreOrder.PreOrderSalesDetailAddOnStruct()
        PreOrderAddOnLollipop.intLineNo = PreOrderSalesHeader.SalesDetailList[intRowIndex].intLineNo
        PreOrderAddOnLollipop.intAddOnLineNo = (PreOrderSalesDetail.AddOnDetailList.count == 0 ? 1 : PreOrderSalesDetail.AddOnDetailList.last!.intAddOnLineNo+1)
        PreOrderAddOnLollipop.strItemType = pAddOnDetail.strItemType
        PreOrderAddOnLollipop.strItemCode = pAddOnDetail.strTypeCode
        PreOrderAddOnLollipop.strCustRemark = ""
        PreOrderAddOnLollipop.decUnitPrice = pAddOnDetail.decSalesAmtFm
        PreOrderAddOnLollipop.decSalesQty = 1
        PreOrderAddOnLollipop.decSalesAmt = pAddOnDetail.decSalesAmtFm
        PreOrderSalesDetail.AddOnDetailList.append(PreOrderAddOnLollipop)
        CollectionViewAddOn.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch(collectionView.restorationIdentifier)
        {
        case "Print":
            return CakeAttributePrint.count
        case "Flavor":
            return CakeAttributeFlavor.count
        case "Size":
            return CakeAttributeSize.count
        case "Lollipop":
            return PreOrderCakeLollipopList.Detail.count
        case "Message":
            return PreOrderCakeLollipopMessageList.Detail.count
        case "Topper":
            return PreOrderCakeTopperList.Detail.count
        case "AddOn":
            return PreOrderSalesDetail.AddOnDetailList.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch(collectionView.restorationIdentifier)
        {
        case "Print":
            let objCollectionViewCell : frmPreOrderItemCakeAttributeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Print", for: indexPath) as! frmPreOrderItemCakeAttributeCell
            objCollectionViewCell.DataBind(pstrAttribute: CakeAttributePrint[indexPath.row])
            return objCollectionViewCell
        case "Flavor":
            let objCollectionViewCell : frmPreOrderItemCakeAttributeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Flavor", for: indexPath) as! frmPreOrderItemCakeAttributeCell
            objCollectionViewCell.DataBind(pstrAttribute: CakeAttributeFlavor[indexPath.row])
            return objCollectionViewCell
        case "Size":
            let objCollectionViewCell : frmPreOrderItemCakeAttributeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Size", for: indexPath) as! frmPreOrderItemCakeAttributeCell
            objCollectionViewCell.DataBind(pstrAttribute: CakeAttributeSize[indexPath.row])
            return objCollectionViewCell
        case "Lollipop":
            let objCollectionViewCell : frmPreOrderItemCakeLollipop = collectionView.dequeueReusableCell(withReuseIdentifier: "Lollipop", for: indexPath) as! frmPreOrderItemCakeLollipop
            objCollectionViewCell.DataBind(pPreOrderItemLollipop: PreOrderCakeLollipopList.Detail[indexPath.row])
            return objCollectionViewCell
        case "Message":
            let objCollectionViewCell : frmPreOrderItemCakeMessage = collectionView.dequeueReusableCell(withReuseIdentifier: "Message", for: indexPath) as! frmPreOrderItemCakeMessage
            objCollectionViewCell.DataBind(pPreOrderItemCakeMessage: PreOrderCakeLollipopMessageList.Detail[indexPath.row])
            return objCollectionViewCell
        case "Topper":
            let objCollectionViewCell : frmPreOrderItemCakeTopper = collectionView.dequeueReusableCell(withReuseIdentifier: "Topper", for: indexPath) as! frmPreOrderItemCakeTopper
            objCollectionViewCell.DataBind(pPreOrderItemTopper: PreOrderCakeTopperList.Detail[indexPath.row])
            return objCollectionViewCell
        case "AddOn":
            let objCollectionViewCell : frmPreOrderItemCakeAddOn = collectionView.dequeueReusableCell(withReuseIdentifier: "AddOn", for: indexPath) as! frmPreOrderItemCakeAddOn
            objCollectionViewCell.DataBind(pPreOrderSalesDetailAddOn: PreOrderSalesDetail.AddOnDetailList[indexPath.row])
            
            objCollectionViewCell.ButtonRemove.tag = indexPath.row
            objCollectionViewCell.ButtonRemove.addTarget(self, action: #selector(self.cmdItemAddOnRemove_Click), for: .touchUpInside)
            return objCollectionViewCell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch(collectionView.restorationIdentifier)
        {
        case "Print":
            var intLocationY : Int = 0
            intLocationY = intLocationY + 80 * Int(ceil(Double(CakeAttributePrint.count) / 2.0)) + 30
            ScrollViewAttribute.setContentOffset(CGPoint(x: 0, y: intLocationY), animated: true)
            
            let objCollectionViewCell : frmPreOrderItemCakeAttributeCell = collectionView.cellForItem(at: indexPath) as! frmPreOrderItemCakeAttributeCell
            objCollectionViewCell.Selected()
            DataBind_LabelItemAttributes()
            break
        case "Flavor":
            var intLocationY : Int = 0
            intLocationY = intLocationY + 80 * Int(ceil(Double(CakeAttributePrint.count) / 2.0)) + 30
            intLocationY = intLocationY + 80 * Int(ceil(Double(CakeAttributeFlavor.count) / 2.0)) + 30
            ScrollViewAttribute.setContentOffset(CGPoint(x: 0, y: intLocationY), animated: true)
            
            let objCollectionViewCell : frmPreOrderItemCakeAttributeCell = collectionView.cellForItem(at: indexPath) as! frmPreOrderItemCakeAttributeCell
            objCollectionViewCell.Selected()
            DataBind_LabelItemAttributes()
            break
        case "Size":
            let objCollectionViewCell : frmPreOrderItemCakeAttributeCell = collectionView.cellForItem(at: indexPath) as! frmPreOrderItemCakeAttributeCell
            objCollectionViewCell.Selected()
            DataBind_LabelItemAttributes()
            break
        case "Lollipop":
            let objCollectionViewCell : frmPreOrderItemCakeLollipop = collectionView.cellForItem(at: indexPath) as! frmPreOrderItemCakeLollipop
            objCollectionViewCell.Selected()
            break
        case "Message":
            let objCollectionViewCell : frmPreOrderItemCakeMessage = collectionView.cellForItem(at: indexPath) as! frmPreOrderItemCakeMessage
            if (objCollectionViewCell.intChildCount == 0)
            {
                objCollectionViewCell.Selected()
            }
            else
            {
                PreOrderCakeLollipopMessagePath.append(objCollectionViewCell.strMessageID)
                PreOrderCakeLollipopMessageList.Read(pstrParentID : PreOrderCakeLollipopMessagePath.last!)
                CollectionViewMessage.DeselectAll()
                CollectionViewMessage.reloadData()
            }
            break
        case "Topper":
            let objCollectionViewCell : frmPreOrderItemCakeTopper = collectionView.cellForItem(at: indexPath) as! frmPreOrderItemCakeTopper
            DataBind_Topper(pAddOnDetail: PreOrderCakeTopperList.Detail[indexPath.row  ])
            break
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        switch(collectionView.restorationIdentifier)
        {
        case "Print","Flavor","Size":
            let objCollectionViewCell : frmPreOrderItemCakeAttributeCell = collectionView.cellForItem(at: indexPath) as! frmPreOrderItemCakeAttributeCell
            objCollectionViewCell.Deselected()
            break
        case "Lollipop":
            let objCollectionViewCell : frmPreOrderItemCakeLollipop = collectionView.cellForItem(at: indexPath) as! frmPreOrderItemCakeLollipop
            objCollectionViewCell.Deselected()
            break
        case "Message":
            let objCollectionViewCell : frmPreOrderItemCakeMessage = collectionView.cellForItem(at: indexPath) as! frmPreOrderItemCakeMessage
            objCollectionViewCell.Deselected()
            break
        default:
            break
        }
    }
    
    @objc func cmdItemAddOnRemove_Click(sender : RoundUIButton) {
        PreOrderSalesDetail.AddOnDetailList.remove(at: sender.tag)
        CollectionViewAddOn.reloadData()
    }
    
    @IBAction func cmdItemClose_Click(sender : UIButton) {
        if let frmPreOrderItemList = self.parent as? frmPreOrderItemList
        {
            frmPreOrderItemList.frmPreOrderItemDetail_Close()
        }
    }
    
    @IBAction func cmdItemBack_Click(sender : UIButton) {
        print("cmdItemBack_Click \(strDetailStep)")
        switch (strDetailStep)
        {
        case "Lollipop":
            ButtonBack.isHidden = true
            ScrollViewAttribute.isHidden = false
            ViewLollipop.isHidden = true
            ViewMessage.isHidden = true
            ViewTopper.isHidden = true
            strDetailStep = "Attribute"
            break
        case "Message":
            if (PreOrderCakeLollipopMessagePath.count == 1)
            {
                ScrollViewAttribute.isHidden = true
                ViewLollipop.isHidden = false
                ViewMessage.isHidden = true
                ViewTopper.isHidden = true
                strDetailStep = "Lollipop"
            }
            else
            {
                PreOrderCakeLollipopMessagePath.removeLast()
                PreOrderCakeLollipopMessageList.Read(pstrParentID : PreOrderCakeLollipopMessagePath.last!)
                CollectionViewMessage.DeselectAll()
                CollectionViewMessage.reloadData()
            }
            break
        case "Topper":
            ScrollViewAttribute.isHidden = true
            ViewLollipop.isHidden = true
            ViewMessage.isHidden = false
            ViewTopper.isHidden = true
            strDetailStep = "Topper"
            ButtonNext.titleLabel!.text = "Next"
            break
        default:
            break
        }
    }
        
    @IBAction func cmdItemNext_Click(sender : UIButton) {
        print("cmdItemNext_Click \(strDetailStep)")
        switch (strDetailStep)
        {
        case "Attribute":
            if ((CollectionViewAttributePrintView.isHidden == false && CollectionViewAttributePrint.indexPathsForSelectedItems!.count == 0) ||
                (CollectionViewAttributeFlavorView.isHidden == false && CollectionViewAttributeFlavor.indexPathsForSelectedItems!.count == 0) ||
                (CollectionViewAttributeSizeView.isHidden == false && CollectionViewAttributeSize.indexPathsForSelectedItems!.count == 0))
            {
                let alertController = UIAlertController(title :"Alert", message:"select cake。", preferredStyle: UIAlertController.Style.alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                ButtonBack.isHidden = false
                ScrollViewAttribute.isHidden = true
                ViewLollipop.isHidden = false
                ViewMessage.isHidden = true
                ViewTopper.isHidden = true
                PreOrderSalesDetail.strItemType = "Cake"
                PreOrderSalesDetail.strItemCode = PreOrderItemCake.CakeItemList.first!.strItemCode
                PreOrderSalesDetail.strItemDesc = PreOrderItemCake.CakeItemList.first!.strItemDesc
                PreOrderSalesDetail.strItemRemark = PreOrderItemCake.CakeItemList.first!.strItemRemark
                PreOrderSalesDetail.decUnitPrice = PreOrderItemCake.CakeItemList.first!.decSalesAmt
                PreOrderSalesDetail.decSalesQty = 1
                PreOrderSalesDetail.decSalesAmt = PreOrderItemCake.CakeItemList.first!.decSalesAmt
                strDetailStep = "Lollipop"
            }
            break
        case "Lollipop":
            if (CollectionViewLollipop.indexPathsForSelectedItems!.count == 0)
            {
                let alertController = UIAlertController(title :"Alert", message:"select cake。", preferredStyle: UIAlertController.Style.alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                let LollipopIndexRow : Int = CollectionViewLollipop.indexPathsForSelectedItems!.first!.row
                if(PreOrderCakeLollipopList.Detail[LollipopIndexRow].strTypeCode == "N/A")
                {
                    ScrollViewAttribute.isHidden = true
                    ViewLollipop.isHidden = true
                    ViewMessage.isHidden = true
                    ViewTopper.isHidden = false
                    CollectionViewLollipop.reloadData()
                    strDetailStep = "Topper"
                }
                else
                {
                    ScrollViewAttribute.isHidden = true
                    ViewLollipop.isHidden = true
                    ViewMessage.isHidden = false
                    ViewTopper.isHidden = true
                    CollectionViewMessage.reloadData()
                    strDetailStep = "Message"
                }
                DataBind_Lollipop(pAddOnDetail: PreOrderCakeLollipopList.Detail[LollipopIndexRow])
            }
            break
        case "Message":
            if (CollectionViewMessage.indexPathsForSelectedItems!.count == 0)
            {
                let alertController = UIAlertController(title :"Alert", message:"select message。", preferredStyle: UIAlertController.Style.alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
            else
            {
                let MessageIndexRow : Int = CollectionViewMessage.indexPathsForSelectedItems!.first!.row
                if(PreOrderCakeLollipopMessageList.Detail[MessageIndexRow].intChildCount != 0)
                {
                    let alertController = UIAlertController(title :"Alert", message:"select message。", preferredStyle: UIAlertController.Style.alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
                else
                {
                    PreOrderSalesDetail.AddOnDetailList[0].strCustRemark = PreOrderCakeLollipopMessageList.Detail[MessageIndexRow].strMessageDesc
                    CollectionViewAddOn.reloadData()
                    ScrollViewAttribute.isHidden = true
                    ViewLollipop.isHidden = true
                    ViewMessage.isHidden = true
                    ViewTopper.isHidden = false
                    CollectionViewLollipop.reloadData()
                    strDetailStep = "Topper"
                }
            }
            break
        case "Topper":
            PreOrderSalesHeader.SalesDetailList[intRowIndex] = PreOrderSalesDetail
            PreOrderSalesHeader.Update()
            if let frmPreOrderItemList = self.parent as? frmPreOrderItemList
            {
                frmPreOrderItemList.frmPreOrderItemDetail_Save(pPreOrderSalesHeader : PreOrderSalesHeader, pstrItemType: "Cake")
            }
            break
        default:
            break
        }
    }
}

class frmPreOrderItemCakeAttributeCell : RoundUICollectionViewCell
{
    @IBOutlet var LabelAttributeDesc : UILabel!
    
    func DataBind(pstrAttribute : String) {
        LabelAttributeDesc.text = pstrAttribute
    }
    
    func Selected(){
        borderColor = UIColor(red: 0/255, green: 74/255, blue: 212/255, alpha: 1.0)
        LabelAttributeDesc.textColor = UIColor(red: 42/255, green: 42/255, blue: 48/255, alpha: 1.0)
    }
    
    func Deselected(){
        borderColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
        LabelAttributeDesc.textColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
    }
}

class frmPreOrderItemCakeLollipop : RoundUICollectionViewCell
{
    @IBOutlet var LabelItemCode : RoundUILabel!
    @IBOutlet var ImageViewLollipop : RoundUIImageView!
    
    func DataBind(pPreOrderItemLollipop : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct) {
        if (pPreOrderItemLollipop.strTypeCode == "N/A")
        {
            var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
            tblPOSItemImageList.Read(pstrTypeCode: "NotApply")
            LabelItemCode.text = ""
            ImageViewLollipop.image = tblPOSItemImageList.Detail.first?.GetUIImage()
        }
        else
        {
            var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
            tblPOSItemImageList.Read(pstrTypeCode: pPreOrderItemLollipop.strTypeCode)
            LabelItemCode.text = pPreOrderItemLollipop.strTypeCode
            ImageViewLollipop.image = tblPOSItemImageList.Detail.first?.GetUIImage()
        }
    }
    
    func Selected(){
        borderColor = UIColor(red: 0/255, green: 74/255, blue: 212/255, alpha: 1.0)
    }
    
    func Deselected(){
        borderColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)    }
}

class frmPreOrderItemCakeMessage : RoundUICollectionViewCell
{
    var strMessageID : String = ""
    var intChildCount : Int64 = 0
    
    @IBOutlet var LabelMessageDesc : RoundUILabel!
    
    func DataBind(pPreOrderItemCakeMessage : StructMaster.PreOrder.PreOrderItemCakeMessageStructList.PreOrderItemCakeMessageStruct) {
        strMessageID = pPreOrderItemCakeMessage.strMessageID
        intChildCount = pPreOrderItemCakeMessage.intChildCount
        LabelMessageDesc.text = pPreOrderItemCakeMessage.strMessageDesc
    }
    
    func Selected(){
        borderColor = UIColor(red: 0/255, green: 74/255, blue: 212/255, alpha: 1.0)
        LabelMessageDesc.textColor = UIColor(red: 42/255, green: 42/255, blue: 48/255, alpha: 1.0)
    }
    
    func Deselected(){
        borderColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
        LabelMessageDesc.textColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
    }
}

class frmPreOrderItemCakeTopper : RoundUICollectionViewCell
{
    @IBOutlet var LabelItemCode : RoundUILabel!
    @IBOutlet var LabelItemDesc : RoundUILabel!
    @IBOutlet var LabelItemAmt : RoundUILabel!
    @IBOutlet var ImageViewTopper : RoundUIImageView!
    
    func DataBind(pPreOrderItemTopper : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct) {
        var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
        tblPOSItemImageList.Read(pstrTypeCode: pPreOrderItemTopper.strTypeCode)
        LabelItemCode.text = pPreOrderItemTopper.strTypeCode
        LabelItemDesc.text = pPreOrderItemTopper.strTypeDesc
        LabelItemAmt.text = GetAmtString(pAmt: pPreOrderItemTopper.decSalesAmtFm)
        ImageViewTopper.image = tblPOSItemImageList.Detail.first?.GetUIImage()
    }
}

class frmPreOrderItemCakeAddOn : RoundUICollectionViewCell
{
    @IBOutlet var ButtonRemove : RoundUIButton!
    @IBOutlet var ImageViewAddOn : RoundUIImageView!
    @IBOutlet var LabelCustRemark : RoundUILabel!
    
    func DataBind(pPreOrderSalesDetailAddOn : StructMaster.PreOrder.PreOrderSalesDetailAddOnStruct) {
        var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
        tblPOSItemImageList.Read(pstrTypeCode: pPreOrderSalesDetailAddOn.strItemCode)
        ImageViewAddOn.image = tblPOSItemImageList.Detail.first?.GetUIImage()
        LabelCustRemark.text = pPreOrderSalesDetailAddOn.strCustRemark
    }
}
