//
//  frmPreOrderItemGiftBox.swift
//  DalloyauPreOrder
//
//  Created by jerry on 26/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderItemGiftBox : UIViewController
{
    private var strItemType : String = ""
    private var PreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct = StructMaster.PreOrder.PreOrderSalesHeaderStruct()
    private var intRowIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func cmdItemClose_Click(sender : UIButton){
        if let frmPreOrderItemList = self.parent as? frmPreOrderItemList
        {
            frmPreOrderItemList.frmPreOrderItemDetail_Close()
        }
    }
    
    @IBAction func cmdItemSave_Click(sender : UIButton){
        if let frmPreOrderItemList = self.parent as? frmPreOrderItemList
        {
            frmPreOrderItemList.frmPreOrderItemDetail_Save(pPreOrderSalesHeader : PreOrderSalesHeader, pstrItemType : strItemType)
        }
    }
}

