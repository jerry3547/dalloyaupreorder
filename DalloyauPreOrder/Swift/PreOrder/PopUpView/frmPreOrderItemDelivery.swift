//
//  frmPreOrderItemDelivery.swift
//  DalloyauPreOrder
//
//  Created by jerry on 29/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderItemDelivery : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    var decItemDeliveryQty : Double = 0.0
    var PreOrderItemDeliveryList : StructMaster.PreOrder.PreOrderItemStructList = StructMaster.PreOrder.PreOrderItemStructList()
    
    // Interface Item //
    @IBOutlet var ContainerViewItemDeliveryMethod : UIView!
    @IBOutlet var ContainerViewItemDeliveryCount : UIView!
    @IBOutlet var ContainerViewItemDeliveryDetail : UIView!
    @IBOutlet var CollectionViewItemDelivery : UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PreOrderItemDeliveryList.Read(pstrItemType: "Delivery")
        
        ContainerViewItemDeliveryMethod.frame = CGRect(x: 112, y: 84, width: 800, height: 600)
        ContainerViewItemDeliveryCount.frame = CGRect(x: 112, y: 84, width: 800, height: 600)
        ContainerViewItemDeliveryDetail.frame = CGRect(x: 112, y: 84, width: 800, height: 600)
        
        ContainerViewItemDeliveryMethod.isHidden = false
        ContainerViewItemDeliveryCount.isHidden = true
        ContainerViewItemDeliveryDetail.isHidden = true
        
        CollectionViewItemDelivery.dataSource = self
        CollectionViewItemDelivery.delegate = self
        CollectionViewItemDelivery.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PreOrderItemDeliveryList.Detail.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objCollectionViewCell : frmPreOrderItemDeliveryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Item", for: indexPath) as! frmPreOrderItemDeliveryCell
        objCollectionViewCell.DataBind(PreOrderItem: PreOrderItemDeliveryList.Detail[indexPath.row])
        return objCollectionViewCell
    }
    
    @IBAction func cmdItemClose_Click(sender : UIButton){
        if let frmPreOrderSummary = self.parent as? frmPreOrderSummary
        {
            frmPreOrderSummary.frmPreOrderItemDelivery_Close()
        }
    }
    
    @IBAction func cmdItemDeliveryBack_Click(sender : UIButton) {
        if (ContainerViewItemDeliveryDetail.isHidden == false) {
            ContainerViewItemDeliveryMethod.isHidden = true
            ContainerViewItemDeliveryCount.isHidden = false
            ContainerViewItemDeliveryDetail.isHidden = true
        }
        else if (ContainerViewItemDeliveryCount.isHidden == false) {
            ContainerViewItemDeliveryMethod.isHidden = false
            ContainerViewItemDeliveryCount.isHidden = true
            ContainerViewItemDeliveryDetail.isHidden = true
        }
    }
    
    @IBAction func cmdItemDeliveryNext_Click(sender : UIButton) {
        let objViewController : frmPreOrderPayment = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderPayment") as! frmPreOrderPayment
        self.present(objViewController, animated: false, completion: nil)
    }
    
    @IBAction func cmdItemDeliveryMethod_Click(sender : UIButton) {
        switch(sender.restorationIdentifier)
        {
        case "Pickup":
            let objViewController : frmPreOrderPayment = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderPayment") as! frmPreOrderPayment
            self.present(objViewController, animated: false, completion: nil)
            break
        case "Delivery":
            ContainerViewItemDeliveryMethod.isHidden = true
            ContainerViewItemDeliveryCount.isHidden = false
            ContainerViewItemDeliveryDetail.isHidden = true
            break
        default:
            break
        }
    }
    
    @IBAction func cmdItemDeliveryCount_Click(sender : UIButton) {
        switch(sender.restorationIdentifier)
        {
        case "1", "2","3","4":
            ContainerViewItemDeliveryMethod.isHidden = true
            ContainerViewItemDeliveryCount.isHidden = true
            ContainerViewItemDeliveryDetail.isHidden = false
            decItemDeliveryQty = sender.restorationIdentifier!.toDouble()
            break
        case "More":
            ContainerViewItemDeliveryMethod.isHidden = true
            ContainerViewItemDeliveryCount.isHidden = true
            ContainerViewItemDeliveryDetail.isHidden = false
            break
        default:
            break
        }
    }
    
}

class frmPreOrderItemDeliveryCell : RoundUICollectionViewCell{
    @IBOutlet var LabelItemDesc : UILabel!
    @IBOutlet var TextViewItemRemark : UITextView!
    @IBOutlet var LabelItemAmt : UILabel!
    
    func DataBind(PreOrderItem : StructMaster.PreOrder.PreOrderItemStructList.PreOrderItemStruct) {
        LabelItemDesc.text = PreOrderItem.strTypeDesc
        TextViewItemRemark.text = PreOrderItem.strTypeRemark
        LabelItemAmt.text = GetAmtString(pAmt: PreOrderItem.decSalesAmtFm)
    }
}
