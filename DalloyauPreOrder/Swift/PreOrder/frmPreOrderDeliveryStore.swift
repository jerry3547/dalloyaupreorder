//
//  frmPreOrderDeliveryStore.swift
//  DalloyauPreOrder
//
//  Created by jerry on 27/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderDeliveryStore : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    // Declare //
    var tblPOSStore : StructMaster.Table.tblPOSStoreStructList = StructMaster.Table.tblPOSStoreStructList()
    
    // Interface //
    @IBOutlet var CollectionViewStore : UICollectionView!
    
    @IBOutlet var ViewNavigationBarHeader : UIView!
    var NavigationBarHeader : frmPreOrderNavigationHeader = frmPreOrderNavigationHeader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblPOSStore.Read()
        
        CollectionViewStore.dataSource = self
        CollectionViewStore.delegate = self
        CollectionViewStore.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is frmPreOrderNavigationHeader
        {
            NavigationBarHeader = segue.destination as! frmPreOrderNavigationHeader
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tblPOSStore.Detail.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objCollectionViewCell : frmPreOrderDeliveryStoreCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Store", for: indexPath) as! frmPreOrderDeliveryStoreCell
        objCollectionViewCell.DataBind(ptblPOSStore: tblPOSStore.Detail[indexPath.row])
        return objCollectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objCollectionViewCell : frmPreOrderDeliveryStoreCell = collectionView.cellForItem(at: indexPath) as! frmPreOrderDeliveryStoreCell
        objCollectionViewCell.Selected()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let objCollectionViewCell : frmPreOrderDeliveryStoreCell = collectionView.cellForItem(at: indexPath) as! frmPreOrderDeliveryStoreCell
        objCollectionViewCell.Deselected()
    }
    
    func ContainerViewSystemMenuShow(pbitIsHidden : Bool) {
        if (pbitIsHidden == true)
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 50)
        }
        else
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        }
    }
    
    @IBAction func cmdPreOrderBack_Click(sender : UIButton){
        let objViewController : frmPreOrderDeliveryMethod = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderDeliveryMethod") as! frmPreOrderDeliveryMethod
        self.present(objViewController, animated: false, completion: nil)
    }
    
    @IBAction func cmdPreOrderNext_Click(sender : UIButton){
        let objViewController : frmPreOrderDeliveryDateTime = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderDeliveryDateTime") as! frmPreOrderDeliveryDateTime
        self.present(objViewController, animated: false, completion: nil)
    }
}

class frmPreOrderDeliveryStoreCell : RoundUICollectionViewCell
{
    @IBOutlet var ImageViewStoreImage : UIImageView!
    @IBOutlet var LabelStoreDesc : UILabel!
    @IBOutlet var LabelStoreAddress : UILabel!
    @IBOutlet var LabelStorePhone : UILabel!
    @IBOutlet var LabelStoreBusiness : UILabel!
    
    func DataBind(ptblPOSStore : StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct) {
        LabelStoreDesc.text = ptblPOSStore.strStoreDesc
        LabelStoreAddress.text = ptblPOSStore.strStoreAddress
        LabelStorePhone.text = ptblPOSStore.strStorePhone
        LabelStoreBusiness.text = ptblPOSStore.strStoreBusinessHour
        ImageViewStoreImage.image = ptblPOSStore.GetUIImage()
    }
    
    func Selected(){
        borderColor = UIColor(red: 0/255, green: 74/255, blue: 212/255, alpha: 1.0)
        LabelStoreDesc.textColor = UIColor(red: 42/255, green: 42/255, blue: 48/255, alpha: 1.0)
        LabelStoreAddress.textColor = UIColor(red: 107/255, green: 108/255, blue: 109/255, alpha: 1.0)
        LabelStorePhone.textColor = UIColor(red: 107/255, green: 108/255, blue: 109/255, alpha: 1.0)
        LabelStoreBusiness.textColor = UIColor(red: 107/255, green: 108/255, blue: 109/255, alpha: 1.0)
    }
    
    func Deselected(){
        borderColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
        LabelStoreDesc.textColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
        LabelStoreAddress.textColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
        LabelStorePhone.textColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
        LabelStoreBusiness.textColor = UIColor(red: 190/255, green: 190/255, blue: 190/255, alpha: 1.0)
    }
}
