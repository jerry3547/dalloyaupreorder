//
//  frmPreOrderDeliveryMethod.swift
//  DalloyauPreOrder
//
//  Created by jerry on 27/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderDeliveryMethod : UIViewController
{
    @IBOutlet var ViewNavigationBarHeader : UIView!
    var NavigationBarHeader : frmPreOrderNavigationHeader = frmPreOrderNavigationHeader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is frmPreOrderNavigationHeader
        {
            NavigationBarHeader = segue.destination as! frmPreOrderNavigationHeader
        }
    }
    
    func ContainerViewSystemMenuShow(pbitIsHidden : Bool) {
        if (pbitIsHidden == true)
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 50)
        }
        else
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        }
    }
    
    @IBAction func cmdPreOrderBack_Click(sender : UIButton) {
        let objViewController : frmPreOrderCustInfo = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderCustInfo") as! frmPreOrderCustInfo
        self.present(objViewController, animated: false, completion: nil)
    }
    
    @IBAction func cmdPreOrderPickupFully_Click(sender : UIButton) {
        let objViewController : frmPreOrderDeliveryStore = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderDeliveryStore") as! frmPreOrderDeliveryStore
        self.present(objViewController, animated: false, completion: nil)
    }
    
    @IBAction func cmdPreOrderPickupPartial_Click(sender : UIButton) {
        let objViewController : frmPreOrderDeliveryItem = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderDeliveryItem") as! frmPreOrderDeliveryItem
        self.present(objViewController, animated: false, completion: nil)
    }
    
    @IBAction func cmdPreOrderDeliveryFully_Click(sender : UIButton) {
        let objViewController : frmPreOrderDeliveryAddress = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderDeliveryAddress") as! frmPreOrderDeliveryAddress
        self.present(objViewController, animated: false, completion: nil)
    }
    
    @IBAction func cmdPreOrderDeliveryPartial_Click(sender : UIButton) {
        let objViewController : frmPreOrderDeliveryItem = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderDeliveryItem") as! frmPreOrderDeliveryItem
        self.present(objViewController, animated: false, completion: nil)
    }
}
