//
//  frmPreOrderPayment.swift
//  DalloyauPreOrder
//
//  Created by jerry on 27/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderPayment : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    // Interface - 1 Confirm //
    @IBOutlet var ContainerViewConfirmItem : UIView!
    @IBOutlet var CollectionViewPreOrderDetail : UICollectionView!
    
    // Interface - 2 Call Bindo Payment Process //
    @IBOutlet var ContainerViewPaymentProcess : UIView!
    
    // Interface - 3 Call API Check status //
    @IBOutlet var ContainerViewCheckPayment : UIView!
    
    @IBOutlet var ViewNavigationBarHeader : UIView!
    var NavigationBarHeader : frmPreOrderNavigationHeader = frmPreOrderNavigationHeader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ContainerViewConfirmItem.frame = CGRect(x: 107, y: 100, width: 800, height: 400)
        ContainerViewPaymentProcess.frame = CGRect(x: 107, y: 100, width: 800, height: 400)
        ContainerViewCheckPayment.frame = CGRect(x: 107, y: 100, width: 800, height: 400)
        
        ContainerViewConfirmItem.isHidden = false
        ContainerViewPaymentProcess.isHidden = true
        ContainerViewCheckPayment.isHidden = true
        
        CollectionViewPreOrderDetail.dataSource = self
        CollectionViewPreOrderDetail.delegate = self
        CollectionViewPreOrderDetail.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is frmPreOrderNavigationHeader
        {
            NavigationBarHeader = segue.destination as! frmPreOrderNavigationHeader
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objCollectionViewCell : frmPreOrderPaymentCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Item", for: indexPath) as! frmPreOrderPaymentCell
        return objCollectionViewCell
    }
    
    @IBAction func cmdConfirmItemNext_Click(sender : UIButton) {
        ContainerViewConfirmItem.isHidden = true
        ContainerViewPaymentProcess.isHidden = false
        ContainerViewCheckPayment.isHidden = true
    }
    
    @IBAction func cmdPaymentProcessNext_Click(sender : UIButton) {
        ContainerViewConfirmItem.isHidden = true
        ContainerViewPaymentProcess.isHidden = true
        ContainerViewCheckPayment.isHidden = false
        
    }
    
    @IBAction func cmdCheckPaymentNext_Click(sender : UIButton) {
        let alertController = UIAlertController(title :"Payment", message:"付款成功，請繼續下一步。", preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            let objViewController : frmPreOrderCustInfo = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderCustInfo") as! frmPreOrderCustInfo
            self.present(objViewController, animated: false, completion: nil)
        }))
        self.present(alertController, animated: true, completion: nil)
    }
}

class frmPreOrderPaymentCell : RoundUICollectionViewCell
{
    
}
