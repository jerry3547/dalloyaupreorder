//
//  frmPreOrderSummary.swift
//  DalloyauPreOrder
//
//  Created by jerry on 27/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmPreOrderSummary : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    var PreOrderSalesHeader : StructMaster.PreOrder.PreOrderSalesHeaderStruct = StructMaster.PreOrder.PreOrderSalesHeaderStruct()
    
    @IBOutlet var ViewNavigationBarHeader : UIView!
    var NavigationBarHeader : frmPreOrderNavigationHeader = frmPreOrderNavigationHeader()
    
    // Interface //
    @IBOutlet var CollectionViewItemSummary : UICollectionView!
    
    // Interface Item //
    @IBOutlet var ContainerViewItemDeteil : UIView!
    @IBOutlet var ContainerViewItemCake : UIView!
    @IBOutlet var ContainerViewItemGiftBox : UIView!
    @IBOutlet var ContainerViewItemDelivery : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CollectionViewItemSummary.dataSource = self
        CollectionViewItemSummary.delegate = self
        CollectionViewItemSummary.reloadData()
        
        ContainerViewItemDeteil.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        ContainerViewItemCake.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        ContainerViewItemGiftBox.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        ContainerViewItemDelivery.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        
        ContainerViewItemDeteil.isHidden = true
        ContainerViewItemCake.isHidden = true
        ContainerViewItemGiftBox.isHidden = true
        ContainerViewItemDelivery.isHidden = true    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is frmPreOrderNavigationHeader
        {
            NavigationBarHeader = segue.destination as! frmPreOrderNavigationHeader
        }
    }
    
    func DataBind(pPreOrderSalesHeader: StructMaster.PreOrder.PreOrderSalesHeaderStruct) {
        PreOrderSalesHeader = pPreOrderSalesHeader
        
        CollectionViewItemSummary.reloadData()
    }
    
    func ContainerViewSystemMenuShow(pbitIsHidden : Bool) {
        if (pbitIsHidden == true)
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 50)
        }
        else
        {
            ViewNavigationBarHeader.frame = CGRect(x: 0, y: 0, width: 1024, height: 768)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PreOrderSalesHeader.SalesDetailList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objCollectionViewCell : frmPreOrderSummaryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Item", for: indexPath) as! frmPreOrderSummaryCell
        objCollectionViewCell.DataBind()
        return objCollectionViewCell
    }
    
    func frmPreOrderItemDelivery_Close() {
        ContainerViewItemDelivery.isHidden = true        
    }
    
    @IBAction func cmdPreOrderBack_Click(sender : UIButton){
        let objViewController : frmPreOrderMainMenu = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderMainMenu") as! frmPreOrderMainMenu
        self.present(objViewController, animated: false, completion: nil)
        objViewController.DataBind(pPreOrderSalesHeader: PreOrderSalesHeader)
    }
    
    @IBAction func cmdPreOrderNext_Click(sender : UIButton){
        ContainerViewItemDelivery.isHidden = false
    }
}

class frmPreOrderSummaryCell : UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate
{
    // Interface //
    @IBOutlet var CollectionViewAddOn : UICollectionView!
    
    func DataBind() {
        CollectionViewAddOn.dataSource = self
        CollectionViewAddOn.delegate = self
        CollectionViewAddOn.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int.random(in: 0...5)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objCollectionViewCell : frmPreOrderSummaryCellAddOn = collectionView.dequeueReusableCell(withReuseIdentifier: "AddOn", for: indexPath) as! frmPreOrderSummaryCellAddOn
        
        return objCollectionViewCell
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}

class frmPreOrderSummaryCellAddOn : UICollectionViewCell
{
    
}
