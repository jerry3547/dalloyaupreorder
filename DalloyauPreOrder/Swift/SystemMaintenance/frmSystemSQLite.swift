//
//  frmSystemSQLite.swift
//  DalloyauPreOrder
//
//  Created by jerry on 28/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmSystemSQLite : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate
{
    // Declare //
    var strTableList : [String] = []
    
    
    // Interface //
    @IBOutlet var CollectionViewTableList : UICollectionView!
    var SystemSQLiteGrid : frmSystemSQLiteGrid = frmSystemSQLiteGrid()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Declare //
        let objSQLite : SQLiteMaster = SQLiteMaster()
        strTableList = objSQLite.GetTableList()
        
        // Interface //
        CollectionViewTableList.dataSource = self
        CollectionViewTableList.delegate = self
        CollectionViewTableList.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is frmSystemSQLiteGrid
        {
            SystemSQLiteGrid = segue.destination as! frmSystemSQLiteGrid
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return strTableList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objCollectionViewCell : frmSystemSQLiteCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Table", for: indexPath) as! frmSystemSQLiteCell
        objCollectionViewCell.DataBind(pstrTableName: strTableList[indexPath.row])
        return objCollectionViewCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let objCollectionViewCell : frmSystemSQLiteCell = collectionView.cellForItem(at: indexPath) as! frmSystemSQLiteCell
        objCollectionViewCell.backgroundColor = UIColor(red: 0/255, green: 123/255, blue: 255/255, alpha: 1.0)
        SystemSQLiteGrid.DataBind(pstrTableName: strTableList[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let objCollectionViewCell : frmSystemSQLiteCell = collectionView.cellForItem(at: indexPath) as! frmSystemSQLiteCell
        objCollectionViewCell.backgroundColor = UIColor.clear
    }
    
    @IBAction func cmdDebugPageClse_Click(sender : UIButton) {
        let objViewController : frmPreOrderMainMenu = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderMainMenu") as! frmPreOrderMainMenu
        self.present(objViewController, animated: true, completion: nil)
    }
}

class frmSystemSQLiteCell : RoundUICollectionViewCell
{
    @IBOutlet var LabelTableName : UILabel!
    
    func DataBind(pstrTableName : String) {
        self.LabelTableName.text = pstrTableName
    }
}
