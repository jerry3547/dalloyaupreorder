//
//  frmSysstemImages.swift
//  DalloyauPreOrder
//
//  Created by jerry on 28/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmSystemItemImages : UIViewController, UICollectionViewDataSource
{
    // Declare //
    var tblPOSItemImageList : StructMaster.Table.tblPOSItemImageStructList = StructMaster.Table.tblPOSItemImageStructList()
    
    // Interface //
    @IBOutlet var CollectionViewImages : UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Declare //
        tblPOSItemImageList.Read()
        
        // Interface
        CollectionViewImages.dataSource = self
        CollectionViewImages.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tblPOSItemImageList.Detail.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objCollectionViewCell : frmSystemItemImagesCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Image", for: indexPath) as! frmSystemItemImagesCell
        objCollectionViewCell.DataBind(ptblPOSItemImage: tblPOSItemImageList.Detail[indexPath.row])
        return objCollectionViewCell
    }
    
    @IBAction func cmdDebugPageClse_Click(sender : UIButton) {
        let objViewController : frmPreOrderMainMenu = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderMainMenu") as! frmPreOrderMainMenu
        self.present(objViewController, animated: true, completion: nil)
    }
}

class frmSystemItemImagesCell : UICollectionViewCell
{
    @IBOutlet var ImageViewItemImage : UIImageView!
    @IBOutlet var LabelItemCode : UILabel!
    
    func DataBind(ptblPOSItemImage : StructMaster.Table.tblPOSItemImageStructList.tblPOSItemImageStruct ){
        LabelItemCode.text = ptblPOSItemImage.strItemCode
        ImageViewItemImage.image = ptblPOSItemImage.GetUIImage()
    }
}
