//
//  frmSystemSQLiteGrid.swift
//  DalloyauPreOrder
//
//  Created by jerry on 28/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmSystemSQLiteGrid: UIViewController
{
    var dataTable: SwiftDataTable! = nil
    var dataTableSource : DataTableContent = []
    var dataTableHeader : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.title = "Streaming fans"
        self.view.backgroundColor = UIColor.white
        
        self.dataTable = SwiftDataTable(dataSource: self)
        self.dataTable.delegate = self
        self.dataTable.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.dataTable.frame = self.view.frame
        self.view.addSubview(self.dataTable);
    }
    
    public func DataBind(pstrTableName : String) {
        let objSQLite : SQLiteMaster = SQLiteMaster()
        let dataTableSourceColumnList : [SQLiteMaster.SQLiteTableColumn] = objSQLite.GetColumnList(pstrTableName: pstrTableName)
        dataTableHeader.removeAll()
        for dataTableSourceColumn : SQLiteMaster.SQLiteTableColumn in dataTableSourceColumnList
        {
            self.dataTableHeader.append(dataTableSourceColumn.strColumnName)
        }
        self.dataTableSource = objSQLite.GetTableData(pstrTableName: pstrTableName)
        if (objSQLite.GetTableRowCount(pstrTableName : pstrTableName) != "0")
        {
            self.dataTable.reload()
        }
    }
}

extension frmSystemSQLiteGrid: SwiftDataTableDataSource {
    public func dataTable(_ dataTable: SwiftDataTable, headerTitleForColumnAt columnIndex: NSInteger) -> String {
        return self.dataTableHeader[columnIndex]
    }
    
    public func numberOfColumns(in: SwiftDataTable) -> Int {
        return self.dataTableHeader.count
    }
    
    func numberOfRows(in: SwiftDataTable) -> Int {
        return self.dataTableSource.count
    }
    
    public func dataTable(_ dataTable: SwiftDataTable, dataForRowAt index: NSInteger) -> [DataTableValueType] {
        return self.dataTableSource[index]
    }
}

extension frmSystemSQLiteGrid: SwiftDataTableDelegate {
    func didSelectItem(_ dataTable: SwiftDataTable, indexPath: IndexPath) {
        print("did select item at indexPath: \(indexPath)")
    }
}
