//
//  frmSystemStoreImages.swift
//  DalloyauPreOrder
//
//  Created by jerry on 28/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//

import Foundation
import UIKit

class frmSystemStoreImages : UIViewController, UICollectionViewDataSource
{
    // Declare //
    var tblPOSStoreList : StructMaster.Table.tblPOSStoreStructList = StructMaster.Table.tblPOSStoreStructList()
    
    // Interface //
    @IBOutlet var CollectionViewImages : UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Declare //
        tblPOSStoreList.Read()
        
        // Interface
        CollectionViewImages.dataSource = self
        CollectionViewImages.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tblPOSStoreList.Detail.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let objCollectionViewCell : frmSystemStoreImagesCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Store", for: indexPath) as! frmSystemStoreImagesCell
        objCollectionViewCell.DataBind(ptblPOSStore: tblPOSStoreList.Detail[indexPath.row])
        return objCollectionViewCell
    }
    
    @IBAction func cmdDebugPageClse_Click(sender : UIButton) {
        let objViewController : frmPreOrderMainMenu = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderMainMenu") as! frmPreOrderMainMenu
        self.present(objViewController, animated: true, completion: nil)
    }
}

class frmSystemStoreImagesCell : UICollectionViewCell
{
    @IBOutlet var ImageViewItemImage : UIImageView!
    @IBOutlet var LabelStoreCode : UILabel!
    
    func DataBind(ptblPOSStore : StructMaster.Table.tblPOSStoreStructList.tblPOSStoreStruct ){
        LabelStoreCode.text = ptblPOSStore.strStoreCode
        ImageViewItemImage.image = ptblPOSStore.GetUIImage()
    }
}

