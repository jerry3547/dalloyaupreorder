//
//  frmLaunchScreen.swift
//  DalloyauPreOrder
//
//  Created by jerry on 26/3/2019.
//  Copyright © 2019 FANCL. All rights reserved.
//
import Foundation
import UIKit

class frmLaunchScreen : UIViewController
{
    @IBOutlet var LabelWebAPIStatus : UILabel!
    var AlertControllerLoading = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
    var strWebAPIStatusMessage : String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let objSQLite : SQLiteMaster = SQLiteMaster()
        objSQLite.CheckTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        self.AlertControllerLoading.title = "Loading..."
        self.AlertControllerLoading.view.addSubview(loadingIndicator)
        
        self.present(self.AlertControllerLoading, animated:true)
        {
            let objWebAPIDataMaster : WebAPIDataMaster = WebAPIDataMaster()
            objWebAPIDataMaster.APIUpdateDeviceInformation()
                {
                    strStatusMessage in
                    DispatchQueue.main.async
                        {
                            self.strWebAPIStatusMessage = strStatusMessage
                            self.LabelWebAPIStatus.text = "\(strStatusMessage)"
                            
                            if (self.strWebAPIStatusMessage == "")
                            {
                                self.dismiss(animated: true, completion: nil)
                                
                                let objSystemImage : SystemIOMaster.SystemImage = SystemIOMaster.SystemImage()
                                objSystemImage.DownloadItemImageFiles()
                                objSystemImage.DownloadStoreImageFiles()
                                
                                let objViewController : frmPreOrderMainMenu = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderMainMenu") as! frmPreOrderMainMenu
                                self.present(objViewController, animated: true, completion: nil)
                            }
                    }
            }
        }
    }
    
    @IBAction func cmdMainMenu_Click(sender : UIButton) {
        let objViewController : frmPreOrderMainMenu = UIStoryboard(name: "PreOrder", bundle: nil).instantiateViewController(withIdentifier: "frmPreOrderMainMenu") as! frmPreOrderMainMenu
        self.present(objViewController, animated: true, completion: nil)
    }
}
